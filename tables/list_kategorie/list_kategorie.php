<?php

class tables_list_kategorie { 

  function beforeSave(&$record) {
    $kateg = $record->val('kategorie');
    if (($kateg == 'Betriebsanweisung') OR ($kateg == 'Sicherheitsdatenblatt') OR ($kateg == 'Zertifikat')) {
        return Dataface_Error::permissionDenied('Die feste Zuordnung '.$kateg.' darf nicht geaendert werden!');
    }
  }

}
?>
