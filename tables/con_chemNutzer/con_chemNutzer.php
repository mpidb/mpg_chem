<?php

class tables_con_chemNutzer { 

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    return $table;  // ohne color
  }

  // glanceview
  function getTitle(&$record) {
    return $record->display('nutzerID');
  }

  function chemID__rendercell(&$record) {
    $name = $record->display('chemID');
    return '<div style="text-align:left;">'.$name.'</div>';
  }

  // bei Usern mit status = 0 wird sonst die ID nur angezeigt wegen vocabulary
  function nutzerID__rendercell(&$record) {
    $name = $record->strval('nutzer');
    return '<div style="text-align:left;">'.$name.'</div>';
  }

  function valuelist__status() {
       return array(0=>'inaktiv', 1=>'aktiv');
  }

}
?>
