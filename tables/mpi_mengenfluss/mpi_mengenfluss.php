<?php

class tables_mpi_mengenfluss { 

  // loeschen nicht erlaubt
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::NO_ACCESS();
    return Dataface_PermissionsTool::getRolePermissions('EDIT');
  }

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    return $table;  // ohne color
  }

  // Anzeige des Feldes im "GlanceView" bei Relationship
  function getTitle(&$record) {
    return $record->display('vorgang').' : '.$record->display('lagerID');
  }

  // fake wegen depselect hidden fields, aber nur in related record
  function tabID__default() {
    $query = Dataface_Application::getInstance()->getQuery();
    if ($query['-table'] == 'mpi_mengenfluss') return NULL;
    $mainRec = Dataface_Application::getInstance()->getRecord();
    return $mainRec->val('tabID');  //set visID equal priID as default
  }

  // Ueberpruefe syntaktische Eingabe im Feld Fluss
  function fluss__validate(&$record, $value, &$params) {
    $tabID = $record->val('chemID');
    $sql = "SELECT SUM(fluss) from mpi_mengenfluss WHERE chemID='$tabID'";
    list($vorh) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    if (!is_numeric($value)) {                                                                                                          
      $params['message'] = 'Fehler: Nur Zahlen hier erlaubt!';                                                                          
      return false;                                                                                                                     
    } elseif (($vorh + $value) < 0) {  
      $params['message'] = 'Fehler: Dieser Wert hat ein negatives Resultat zur Folge! ('.$vorh.' + '.$value.') < 0';
      return false;
    } elseif ($value == 0) {
      $params['message'] = 'Fehler: Eine NULL macht keinen Sinn!';
      return false;
    } else {
    return true;
    }
  }

  function chemID__renderCell(&$record) {
    $table  = 'mpi_chemstoff';
    $action = 'browse';
    $field  = 'chemID';
    $tabID  = $record->val($field);
    $name   = $record->display($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&tabID=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function anzahl__renderCell(&$record) {
    return '<div style="text-align:right">'.$record->display('anzahl').'</div>';
  }

  // Formatiere Zeitstempel auf Deutsch
  function zeitstempel__display(&$record) {
    if ($record->strval('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  function beforeSave(&$record) {
    // User automatisch bei neu und aendern setzen
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);
  }

/*  // ueber cronjob  geregelt
  function afterSave(&$record) {
    // Mail schicken wenn Mindestmenge unterschritten
    $table = 'mpi_chemstoff';
    $tabID = $record->val('chemID');
    $sql = "SELECT SUM(fluss) FROM `mpi_mengenfluss` WHERE chemID='$tabID'";
    list($sum) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $sql = "SELECT min       FROM $table WHERE `tabID`='$tabID'";
    list($min)  = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $sql = "SELECT nachricht FROM $table WHERE `tabID`='$tabID'";
    list($usr)  = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $sql = "SELECT substanz  FROM $table WHERE `tabID`='$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));

    $url = 'http://'.gethostname().DATAFACE_SITE_URL.'/index.php?-table='.$table.'&tabID='.$tabID;
    //return Dataface_Error::permissionDenied('-'.$url.'-');

    if (($min != NULL) AND ($usr != NULL) AND ($usr != '--')) {
      if ($sum <= $min) {
        $betreff = 'Mindestmenge eines Artikels in Tabelle '.$table.' erreicht';
        $text = 'Artikel: '.$name."\r\n".'Minimum: '.$min."\r\n".'Anzahl: '.$sum."\r\n".$url;
        //return Dataface_Error::permissionDenied('-'.$mailusr.'-'.$betreff.'-'.$text.'-');
        mail($usr, $betreff, $text, "From: 'Datenbank Admin' <support@mpi-magdeburg.mpg.de>");
      }
    }
  } 
*/

}
?>
