-- version mpi-dcts
CREATE OR REPLACE VIEW view_lgk AS
SELECT
 autoID,
 lgk,
 beschreibung,
 priority
FROM
 mpidb_tech_gfk.list_lgk
;

-- version mpg
CREATE OR REPLACE VIEW view_lgk AS
SELECT
 autoID,
 lgk,
 beschreibung,
 priority
FROM
 mpidb_mpg_lgk.list_lgk
;
