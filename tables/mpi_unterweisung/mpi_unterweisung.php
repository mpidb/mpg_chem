<?php

class tables_mpi_unterweisung { 

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    return $table;  // ohne color
  }

  // glanceview
  function getTitle(&$record) {
    return $record->display('nutzerID');
  }

  function datum__default() {
    return date("d.m.Y");
  }

  // bei Usern mit status = 0 wird sonst die ID nur angezeigt wegen vocabulary
  function nutzerID__rendercell(&$record) {
    //$field = 'nutzerID'; 
    //$table = 'list_nutzer';
    //$tabID = $record->val($field);
    //$sql = "SELECT nutzer FROM $table WHERE $field='$tabID'";
    //list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $name = $record->strval('nutzer');
    return '<div style="text-align:left;">'.$name.'</div>';
  }

  function datum__display(&$record) {
    if ($record->val('datum') == NULL) return ;
    return date('d.m.Y', strtotime($record->strval('datum')));
  }

  function valuelist__status() {
       return array(0=>'inaktiv', 1=>'aktiv');
  }

  // Formatiere Zeitstempel auf Deutsch
  function zeitstempel__display(&$record) {
    if ($record->val('zeitstempel') == NULL) return ;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  function beforeSave(&$record) {
    // User automatisch bei neu und aendern setzen
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);

    if ($record->val('ablageID') == NULL) {
      return Dataface_Error::permissionDenied('Ohne Betriebsanweisung ist Unterweisung sinnlos!');
    }
  }

}
?>
