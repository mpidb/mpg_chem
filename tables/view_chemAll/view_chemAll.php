<?php

class tables_view_chemAll { 

  // check login & editieren nicht erlaubt
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::NO_ACCESS();
    return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
  }

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    return $table;
  }

  // link nach Substanz
  function substanz__renderCell( &$record ) {
    $subst = $record->strval('substanz');
    $tabID = $record->strval('tabID');
    $subdb = $record->strval('gruppe').'_chem';
    $table = 'mpi_chemstoff';
    return '<div style="white-space:nowrap"><a href="/'.$subdb.'/index.php?-table='.$table.'&-action=browse&tabID='.$tabID.'">'.$subst.'</a></div>';
  }

  // Umbruch in Liste verhindern
  function cas__renderCell(&$record) {
    return '<div style="white-space:nowrap">'.$record->display('cas').'</div>';
  }

  // Umbruch in Liste verhindern !!! verhindert leider auch den link in releationships
  function einheit__renderCell(&$record) {
    return '<div style="white-space:nowrap">'.$record->display('einheit').'</div>';
  }

  function cmr__renderCell(&$record) {
    if ($record->val('cmr') == '1') return 'ja'; else return 'nein';
  }

  function bFilename__renderCell( &$record ) {
    $table = 'mpi_ablage';
    $field = 'ablageID';
    $tabID = $record->val('bFileID');
    $subdb = $record->strval('gruppe').'_chem';
    //$name  = $record->display('bFileID');
    $name  = $record->strval('bFilename');
    return '<div style="text-align:right"><a href="/'.$subdb.'/index.php?-action=getBlob&-table='.$table.'&-field=file&-index=0&'.$field.'='.$tabID.'">'.$name.'</a></div>';
  }

  function sFilename__renderCell( &$record ) {
    $table = 'mpi_ablage';
    $field = 'ablageID';
    $tabID = $record->val('sFileID');
    $subdb = $record->strval('gruppe').'_chem';
    //$name  = $record->display('sFileID');
    $name  = $record->strval('sFilename');
    return '<div style="text-align:right"><a href="/'.$subdb.'/index.php?-action=getBlob&-table='.$table.'&-field=file&-index=0&'.$field.'='.$tabID.'">'.$name.'</a></div>';
  }

  // Formatiere Zeitstempel auf Deutsch
  function zeitstempel__display(&$record)  {
    if ($record->strval('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

}

?>
