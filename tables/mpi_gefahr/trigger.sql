-- triggers mpi_gefahr
DROP TRIGGER IF EXISTS `lgk_update_del`;
DELIMITER //
CREATE TRIGGER `lgk_update_del` AFTER DELETE ON `mpi_gefahr`
 FOR EACH ROW UPDATE mpi_chemstoff SET lgk = (SELECT lgk from view_lgk where priority = (SELECT min(t3.priority) FROM mpi_gefahr AS t1, view_gefahr AS t2, view_lgk AS t3 WHERE t1.kategorie = t2.kategorie AND t1.chemID = OLD.chemID AND t2.lgk = t3.lgk)) where tabID = OLD.chemID
//
DELIMITER ;

DROP TRIGGER IF EXISTS `lgk_update_ins`;
DELIMITER //
CREATE TRIGGER `lgk_update_ins` AFTER INSERT ON `mpi_gefahr`
 FOR EACH ROW UPDATE mpi_chemstoff SET lgk = (SELECT lgk from view_lgk where priority = (SELECT min(t3.priority) FROM mpi_gefahr AS t1, view_gefahr AS t2, view_lgk AS t3 WHERE t1.kategorie = t2.kategorie AND t1.chemID = NEW.chemID AND t2.lgk = t3.lgk)) where tabID = NEW.chemID
//
DELIMITER ;

DROP TRIGGER IF EXISTS `lgk_update_upd` ;
DELIMITER //
CREATE TRIGGER `lgk_update_upd` AFTER UPDATE ON `mpi_gefahr`
 FOR EACH ROW UPDATE mpi_chemstoff SET lgk = (SELECT lgk from view_lgk where priority = (SELECT min(t3.priority) FROM mpi_gefahr AS t1, view_gefahr AS t2, view_lgk AS t3 WHERE t1.kategorie = t2.kategorie AND t1.chemID = OLD.chemID AND t2.lgk = t3.lgk)) where tabID = OLD.chemID
//
DELIMITER ;



-- old mit direkter abfrage ext. db, schlecht fuer export
DROP TRIGGER IF EXISTS `lgk_update_del`;
DELIMITER //
CREATE TRIGGER `lgk_update_del` AFTER DELETE ON `mpi_gefahr`
 FOR EACH ROW UPDATE mpi_chemstoff SET lgk = (SELECT lgk from mpidb_mpg_gfk.list_lgk where priority = (SELECT min(t3.priority) FROM mpi_gefahr AS t1, mpidb_mpg_gfk.list_gefahr AS t2, mpidb_mpg_gfk.list_lgk AS t3 WHERE t1.kategorie = t2.kategorie AND t1.chemID = OLD.chemID AND t2.lgk = t3.lgk)) where tabID = OLD.chemID
//
DELIMITER ;

DROP TRIGGER IF EXISTS `lgk_update_ins`;
DELIMITER //
CREATE TRIGGER `lgk_update_ins` AFTER INSERT ON `mpi_gefahr`
 FOR EACH ROW UPDATE mpi_chemstoff SET lgk = (SELECT lgk from mpidb_mpg_gfk.list_lgk where priority = (SELECT min(t3.priority) FROM mpi_gefahr AS t1, mpidb_mpg_gfk.list_gefahr AS t2, mpidb_mpg_gfk.list_lgk AS t3 WHERE t1.kategorie = t2.kategorie AND t1.chemID = NEW.chemID AND t2.lgk = t3.lgk)) where tabID = NEW.chemID
//
DELIMITER ;

DROP TRIGGER IF EXISTS `lgk_update_upd` ;
DELIMITER //
CREATE TRIGGER `lgk_update_upd` AFTER UPDATE ON `mpi_gefahr`
 FOR EACH ROW UPDATE mpi_chemstoff SET lgk = (SELECT lgk from mpidb_mpg_gfk.list_lgk where priority = (SELECT min(t3.priority) FROM mpi_gefahr AS t1, mpidb_mpg_gfk.list_gefahr AS t2, mpidb_mpg_gfk.list_lgk AS t3 WHERE t1.kategorie = t2.kategorie AND t1.chemID = OLD.chemID AND t2.lgk = t3.lgk)) where tabID = OLD.chemID
//
DELIMITER ;
