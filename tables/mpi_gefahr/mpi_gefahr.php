<?php

class tables_mpi_gefahr { 

  // setze Farbe in listenansicht entsprechend Anweisung           
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    // return $table;  // ohne color
    $tabID = $record->val('kategorie');
    $sql = "SELECT anweisung FROM view_gefahr WHERE kategorie = '$tabID'";
    list($attr) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    if ($attr == '1') return $table.'lineY';
    return $table.'normal';
  }

  function chemID__rendercell(&$record) {
    $table  = 'mpi_chemstoff';
    $action = 'browse';
    $field  = 'chemID';
    $tabID  = $record->val($field);
    $name   = $record->display('chemID');
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&tabID=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function valuelist__cmr() {
    return array(0=>'nein', 1=>'ja');
  }

  function valuelist__anweisung() {
    return array(0=>'nein', 1=>'ja');
  }

  function valuelist__substition() {
    return array(0=>'nein', 1=>'ja');
  }

  // Formatiere Zeitstempel auf Deutsch
  function zeitstempel__display(&$record) {
    if ($record->strval('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  function beforeSave(&$record) {
    // User automatisch bei neu und aendern setzen
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);
  }

/*
// durch trigger in db geloest
  function afterSave(&$record)
  {
    // setze Lagerklasse entsprechend Gefahrenklasse 
    $tabID = $record->val('chemID');
    // hole Mindestwert alle Lagerklassen dieser Substanz
    $sql = "SELECT min(t3.priority) FROM mpi_gefahr AS t1, view_gefahr AS t2, view_lgk AS t3 WHERE t1.kategorie = t2.kategorie AND t1.chemID = '$tabID' AND t2.lgk = t3.lgk";
    list($pri) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    //print_r($pri);
    $sql = "SELECT lgk FROM view_lgk WHERE priority = '$pri'";
    list($lgk) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $sql = "UPDATE mpi_chemstoff SET lgk='$lgk' WHERE mpi_chemstoff.tabID='$tabID'";
    // return Dataface_Error::permissionDenied('-'.$sql.'-');
    $res = xf_db_query($sql, df_db());
  }
*/

}
?>
