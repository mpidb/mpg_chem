-- version mpi-dcts
CREATE OR REPLACE VIEW view_gefahr AS
SELECT
 *
FROM
 mpidb_tech_gfk.list_gefahr
;

-- version mpg 
CREATE OR REPLACE VIEW view_gefahr AS
SELECT
 *
FROM
 mpidb_mpg_gfk.list_gefahr
;

-- version stand alone
CREATE OR REPLACE VIEW view_gefahr AS
 SELECT
  '000001' AS autoID,
  'keine' AS kategorie,
  'H000' AS hsatz,
  'Noch nicht verlinkt nach mpg_gfk.list_gefahr' AS merkmal,
  '0' AS cmr,
  '0' AS anweisung,
  '0' AS substition,
  '0' AS lgk,
  '--' AS kennzeichen
;
