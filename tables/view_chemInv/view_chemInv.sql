-- mit menge pro lager pro chemstoff und anzahl nicht NULL und > 0
-- spezieller View fuer Erleichterung Abgleich mit Formular Datenbestand in Chemie-Lagern
-- Forderung Hermsdorf an Gruppen
CREATE OR REPLACE VIEW view_chemInv AS
SELECT
  (SELECT raum FROM mpi_lagerort WHERE lagerID = vfluss.lagerID) AS Lagerort,
  (SELECT ablage FROM mpi_lagerort WHERE lagerID = vfluss.lagerID) AS Ablage,
  LPAD(chem.tabID,6,'0') AS ChemID,
  chem.substanz AS Substanz,
  chem.reinheit AS Reinheit,
  chem.lgk AS Lagerklasse,
  chem.einheit AS Entnahmegr,
  vfluss.anzahl AS Menge,
  NULL AS vorhanden,
  NULL AS Bemerkung
 FROM
  mpi_chemstoff AS chem
  LEFT JOIN view_mengenfluss as vfluss ON vfluss.chemID = chem.tabID
 WHERE
  vfluss.anzahl IS NOT NULL AND vfluss.anzahl > 0
 ORDER BY
  Lagerort, Ablage, Substanz, Reinheit, Einheit
;

