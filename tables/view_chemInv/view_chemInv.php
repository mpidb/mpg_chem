<?php

class tables_view_chemInv { 

  // only authenticated user; edit in view not allowed; check role lower READ ONLY
  function getRoles(&$record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return 'NO ACCESS';
    return 'READ ONLY';
  }

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = 'view_chemstoff ';
    // return $table.'normal'; //mit color
    return $table;  // ohne color
  }

  // tabID mit nullen auffuellen solange noch nicht in DB geaendert
  function ChemID__display(&$record) {
    $tabID = $record->strval('ChemID');
    return str_pad($tabID, 5, 0, STR_PAD_LEFT);
  }

  // link nach Substanz
  function Substanz__renderCell(&$record) {
    $subst = $record->strval('Substanz');
    $tabID = $record->strval('ChemID');
    $table = 'mpi_chemstoff';
    return '<div style="white-space:nowrap"><a href="'.DATAFACE_SITE_HREF.'?-table='.$table.'&-action=browse&tabID='.$tabID.'">'.$subst.'</a></div>';
  }

  // Fake: weil '<' nicht dargestellt
  function Reinheit__renderCell(&$record) {
    return $record->display('reinheit');
  }

}
?>
