CREATE OR REPLACE VIEW view_mengenfluss AS
SELECT
 chemID,
 lagerID,
 SUM(fluss) AS anzahl
FROM
 mpi_mengenfluss
GROUP BY
 chemID, lagerID
;


