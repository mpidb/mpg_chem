<?php

class tables_con_lagerLgk {

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    return $table;  // ohne color
  }

  // Anzeige in liste aendern
  function lgk__renderCell(&$record) {
    return $record->val('lgk');
  }
 
}
?>
