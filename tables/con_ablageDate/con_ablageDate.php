<?php

class tables_con_ablageDate { 

  // setze Farbe in listenansicht entsprechend Minwert zu Menge
  function css__tableRowClass( &$record ) {
    return 'mpi_ablage ';
  }

  function getTitle(&$record) {
    return $record->strval('reminder');
  }

  function ablageID__renderCell( &$record ) {
    $table = 'mpi_ablage';
    $file  = $record->display('ablageID');
    $field = 'ablageID';
    $tabID = $record->val($field);
    return '<a href="'.DATAFACE_SITE_HREF.'?-action=getBlob&-table='.$table.'&-field=file&-index=0&'.$field.'='.$tabID.'">'.$file.'</a>';
  }

  function reminder__default() {
    $default = date('Y-m-d',mktime(0,0,0,date("m"),date("d"),date("Y")+1));
    return date('d.m.Y', strtotime($default));
  }

  function reminder__display( &$record ) {
    $date = $record->strval('reminder');
    if ($date == NULL) return;
    return date('d.m.Y', strtotime($date));
  }

  // Formatiere Zeitstempel auf Deutsch
  function zeitstempel__display(&$record) {
    if ($record->strval('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);
  }

}
?>
