<?php

class tables_mpi_ablage { 

  // readable for 'VIEW GROUP ..'
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::getRolePermissions('NO ACCESS');
    $role = $user->val('role');
    // specially role VIEW GROUP PSE here and other group NO ACCESS
    if ( $role === 'VIEW GROUP PSE') return Dataface_PermissionsTool::getRolePermissions($role);
    if ( strpos($role,'VIEW GROUP ') !== false) return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
    return Dataface_PermissionsTool::getRolePermissions($role);
  }

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    return $table;  // ohne color
  }

  // glanceview
  function getTitle(&$record) {
    //return $record->strval('file_filename').' : '.$record->strval('bezeichnung');
    return $record->strval('file_filename');
  }

  // Anzeige des Feldes "Add related ... record" bei Relationsship
  function titleColumn() {
    return "CONCAT('[',ablageID,'] ',kategorie,' : ',file_filename,' : ',bezeichnung)";
  }

  // button remove weg
  function rel_chemiestoff_ba__permissions($record) {
    return array( 'update related records' => 0, 'remove related record' => 0 );
  }

  function rel_chemiestoff_sdb__permissions($record) {
    return array( 'update related records' => 0, 'remove related record' => 0 );
  }

  function file_filename__renderCell( &$record ) {
    $table = 'mpi_ablage';
    $field = 'ablageID';
    $tabID = $record->val($field);
    $file  = $record->strval('file_filename');
    //$url = $record->getURL('-action=getBlob');
    return '<a href="'.DATAFACE_SITE_HREF.'?-action=getBlob&-table='.$table.'&-field=file&-index=0&'.$field.'='.$tabID.'">'.$file.'</a>';
  }

   // Zeitanzeige aendern
  function zeitstempel__display(&$record) {
    if ($record->val('zeitstempel') == NULL) {
      return "";
    } else {
      return date('d.m.Y', strtotime($record->strval('zeitstempel')));
    }
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);

    if ($record->strval('file_filename') == NULL) {
      return Dataface_Error::permissionDenied('Was soll denn eine Abage ohne Fileupload!');
    }
  }

}

?>
