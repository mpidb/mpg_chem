<?php

class tables_mpi_chemstoff__history
{ 

  // check login & editieren nicht erlaubt
  function getPermissions($record)
  {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::NO_ACCESS();
    return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
  }

  function cmr__renderCell(&$record)
  {
    if ($record->val('cmr') == '1')
    {
      return 'ja';
    } else {
      return 'nein';
    }
  }

  function gift__renderCell(&$record)
  {
    if ($record->val('gift') == '1')
    {
      return 'ja';
    } else {
      return 'nein';
    }
  }

  // Umbruch in Liste verhindern !!! verhindert leider auch den link in releationships
  function substanz__renderCell(&$record)
  {
    $app = Dataface_Application::getInstance();
    $query =& $app->getQuery();
    $table = $query['-table'];
    if ($table == 'mpi_chemstoff__history')
    {
      return '<div style="white-space:nowrap">'.$record->display('substanz').'</div>';
    } else {
      return;
    }
  }

  // Fake: weil '<' nicht dargestellt
  function reinheit__renderCell(&$record)
  {
    return $record->display('reinheit');
  }

  // Umbruch in Liste verhindern
  function cas__renderCell(&$record)
  {
    return '<div style="white-space:nowrap">'.$record->display('cas').'</div>';
  }

  // link in listview aendern
  function bfile_filename__renderCell( &$record )
  {
    $file  = $record->strval('bfile_filename');
    $blob  = 'bfile';
    $tabID = $record->strval('tabID');
    $app = Dataface_Application::getInstance();
    $query =& $app->getQuery();
    $table = $query['-table'];
    // http://mpidb.mpi-magdeburg.mpg.de/pse_chem/index.php?-action=getBlob&-table=mpi_chemstoff&-field=sfile&-index=0&tabID=1
    return '<a href="'.DATAFACE_SITE_HREF.'?-action=getBlob&-table='.$table.'&-field='.$blob.'&-index=0&tabID='.$tabID.'">'.$file.'</a>';
  }

  // link in listview aendern
  function sfile_filename__renderCell( &$record )
  {
    $file  = $record->strval('sfile_filename');
    $blob  = 'sfile';
    $tabID = $record->strval('tabID');
    $app = Dataface_Application::getInstance();
    $query =& $app->getQuery();
    $table = $query['-table'];
    // http://mpidb.mpi-magdeburg.mpg.de/pse_chem/index.php?-action=getBlob&-table=mpi_chemstoff&-field=sfile&-index=0&tabID=1
    return '<a href="'.DATAFACE_SITE_HREF.'?-action=getBlob&-table='.$table.'&-field='.$blob.'&-index=0&tabID='.$tabID.'">'.$file.'</a>';
  }

  // Anzeige der 0-Werte (nicht verwechseln mit NULL) in details, wird sonst ausgeblendet
  function menge__htmlValue( &$record )
  {
    $value = $record->strval('menge');
    if ($value == 0) return '0 ';
    else return $value;
  }

  // Anzeige der 0-Werte (nicht verwechseln mit NULL) in details, wird sonst ausgeblendet
  function min__htmlValue( &$record )
  {
    $value = $record->strval('min');
    if ($value == 0) return '0 ';
    else return $value;
  }

}
?>
