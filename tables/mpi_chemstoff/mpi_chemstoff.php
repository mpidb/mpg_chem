<?php

class tables_mpi_chemstoff { 

  function block__before_fineprint() {
    $app    = Dataface_Application::getInstance();
    $fs_ver = $app->_conf['_own']['version'];
    $mailto = $app->_conf['_own']['mailto'];
    $mname  = $app->_conf['_own']['mailname'];
    $sql    = "SELECT MAX(version) FROM dataface__version";
    list($db_ver) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $fs_rep = str_replace('.','',$fs_ver);
    if ( $fs_rep == $db_ver )
      echo 'Version <span style="color: green">FS: '.$fs_ver.'</span> = <span style="color: green">DB: '.$db_ver.'</span> &copy;<a href="mailto:'.$mailto.'">'.$mname.'</a>';
  }

  function block__before_body() {
    $app    = Dataface_Application::getInstance();
    $fs_ver = $app->_conf['_own']['version'];
    $sql    = "SELECT MAX(version) FROM dataface__version";
    list($db_ver) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $fs_rep = str_replace('.','',$fs_ver);
    if ( $fs_rep != $db_ver ) {
      if ( $fs_rep > $db_ver ) {
        echo 'Version <span style="color: green">FS: '.$fs_ver.'</span> &ne; <span style="color: red">DB: '.$db_ver.'</span>';
      } else {
        echo 'Version <span style="color: red">FS: '.$fs_ver.'</span> &ne; <span style="color: green">DB: '.$db_ver.'</span>';
      }
    }
  }

  function block__before_menus() {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::NO_ACCESS();
    $urlAdd = DATAFACE_SITE_HREF."?-table=view_chemstoff";
    echo   '<a class="contentActions" href='.$urlAdd.' title="Zeige alle verlinkten Datens&auml;tze"><img src="/xataface/images/zoom-out.gif" alt="" width="16" height="16"> Alle Lagerorte zeigen </a>';
    $urlAdd = DATAFACE_SITE_HREF."?-table=view_chemInv";
    echo   '<a class="contentActions" href='.$urlAdd.' title="Reduzierte Ansicht für Inventur Lagerbestand"><img src="images/inventur.png" alt="" width="14" height="14"> Inventur Lagerbestand </a>';
  }

  function block__after_result_list_content() {
    echo 'Lagerbestand hat den Mindestwert ';
    echo '<span style="background-color:#fcc;"> unterschritten. </span>&nbsp;|&nbsp;';
    echo '<span style="background-color:#ffc;"> erreicht. </span>&nbsp;|&nbsp;';
    echo '<span style="background-color:#cff;"> fast erreicht. </span>&nbsp;|&nbsp;';
    echo '<span style="background-color:#cfc;"> noch nicht erreicht. </span>&nbsp;|&nbsp;';
    echo '<span> nicht ueberwacht. </span>&nbsp;';
  }

  // setze Farbe in listenansicht entsprechend Minwert zu Menge
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    $menge = $record->val('anzahl');
    $min   = $record->val('min');
    $near  = $min + 1;
    if (($min == NULL) OR ($menge == NULL)) return $table;
    elseif ($menge <  $min)  return $table.'rot';
    elseif ($menge == $min)  return $table.'gelb';
    elseif ($menge == $near) return $table.'cyan';
    elseif ($menge >  $min)  return $table.'gruen';
    return $table;
  }

  // button remove in mengenfluss weg
  function rel_mengenfluss__permissions($record) {
    return array( 'remove related record' => 0 );
  }

  // button update in ablageort weg
  function rel_lagerort__permissions($record) {
    //return array( 'remove related record' => 0, 'update related records' => 0 );
    return array( 'update related records' => 0 );
  }

  // glanceview
  function getTitle(&$record) {
    //return $record->display('substanz').' ['.$record->display('tabID').']';
    return $record->strval('substanz');
  }

  function valuelist__gift(){
    return array(0=>'nein', 1=>'ja');
  }

  // tabID mit nullen auffuellen solange noch nicht in DB geaendert
  function tabID__display(&$record) {
    $tabID = $record->strval('tabID');
    return str_pad($tabID, 5, 0, STR_PAD_LEFT);
  }

  // Umbruch in Liste verhindern !!! verhindert leider auch den link in releationships, deshalb nur in mpi_chemstoff
  function substanz__renderCell(&$record) {
    $app = Dataface_Application::getInstance();
    $query =& $app->getQuery();
    $table = $query['-table'];
    if ($table == 'mpi_chemstoff') return '<div style="white-space:nowrap">'.$record->display('substanz').'</div>';
    return;
  }

  function anzahl__renderCell(&$record) {
    return '<div style="text-align:right">'.$record->display('anzahl').'</div>';
  }

  function einheit__renderCell(&$record) {
    return '<div style="text-align:right">'.$record->display('einheit').'</div>';
  }
 
  // Fake: weil '<' nicht dargestellt
  function reinheit__renderCell(&$record) {
    return $record->display('reinheit');
  }

  // Umbruch in Liste verhindern
  function cas__renderCell(&$record) {
    return '<div style="white-space:nowrap">'.$record->display('cas').'</div>';
  }

  // link zum file und zugriff auf lokalen blob
  function bFileID__renderCell( &$record ) {
    $ablageID  = $record->val('bFileID');
    $table = 'mpi_ablage';
    $name  = $record->display('bFileID');
    return '<a href="'.DATAFACE_SITE_HREF.'?-action=getBlob&-table='.$table.'&-field=file&ablageID='.$ablageID.'">'.$name.'</a>';
  }

  // link zum file und zugriff auf lokalen blob
  function sFileID__renderCell( &$record ) {
    $ablageID  = $record->val('sFileID');
    $table = 'mpi_ablage';
    $name  = $record->display('sFileID');
    return '<a href="'.DATAFACE_SITE_HREF.'?-action=getBlob&-table='.$table.'&-field=file&ablageID='.$ablageID.'">'.$name.'</a>';
  }

  // Anzeige der 0-Werte (nicht verwechseln mit NULL) in details, wird sonst ausgeblendet
  function anzahl__htmlValue( &$record ) {
    $value = $record->val('anzahl');
    if ($value == 0) return '0 ';
    return $value;
  }

  // Anzeige der 0-Werte (nicht verwechseln mit NULL) in details, wird sonst ausgeblendet
  function min__htmlValue( &$record ) {
    $value = $record->val('min');
    if ($value == 0) return '0 ';
    return $value;
  }

/*
  function valuelist__cmr() {
    return array(0=>'nein', 1=>'ja');
  }

  function valuelist__anweisung() {
    return array(0=>'nein', 1=>'ja');
  }

  function valuelist__substition() {
    return array(0=>'nein', 1=>'ja');
  }
*/

  // Formatiere Zeitstempel auf Deutsch
  function zeitstempel__display(&$record) {
    if ($record->strval('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);


  }

}
?>
