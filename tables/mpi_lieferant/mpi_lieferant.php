<?php

class tables_mpi_lieferant { 

  // setze Farbe in listenansicht entsprechend Minwert zu Menge
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    return $table;
  }

  function email__renderCell( &$record ) {
    $mail = $record->strval('email');
    if ($mail != NULL) return '<a href="mailto:'.$mail.'">'.$mail.'</a>';
    return $mail;
  }

  function webseite__renderCell( &$record ) {
    $web = $record->strval('webseite');
    if (($web != NULL) AND (strpos($web,'://') === FALSE )) {
      return '<a href="http://'.$web.'">http://'.$web.'</a>';
    }
    return '<a href="'.$web.'">'.$web.'</a>';
  }

  function zeitstempel__display(&$record) {
    if ($record->val('zeitstempel') == NULL) {
      return "";
    } else {
      return date('d.m.Y', strtotime($record->strval('zeitstempel')));
    }
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    // setze user
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);
  }

}
?>
