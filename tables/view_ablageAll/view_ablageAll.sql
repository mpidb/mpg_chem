-- version alone
-- fake auf sich selbst, also ohne db_chem's
CREATE OR REPLACE VIEW view_ablageAll AS
 SELECT 
  '000001' AS ablageID,
  'Betriebsanweisung' AS kategorie,
  'Fake Eintrag wenn keine chemDB' AS bezeichnung,
  'noName' AS filename,
  'mpg' AS gruppe,
  'initial' AS bearbeiter,
  '2016-04-22 11:34:18' AS zeitstempel
;


-- version mpg mit mpg_chem
-- entsprechend erweitern/anpassen, wenn mehr chemie-db's existieren
-- siehe Beispiel mpi-dcts (4db's) in ../tables/view_ablageAll/view_ablageAll.sql 
CREATE OR REPLACE VIEW view_ablageAll AS
 SELECT 
  LPAD(mpg.ablageID, 6, '0') AS ablageID,
  mpg.kategorie AS kategorie,
  mpg.bezeichnung AS bezeichnung,
  mpg.file_filename AS filename,
  'mpg' AS gruppe,
  mpg.bearbeiter AS bearbeiter,
  mpg.zeitstempel AS zeitstempel
 FROM
  mpidb_mpg_chem.mpi_ablage AS mpg
 WHERE
  mpg.kategorie = 'Betriebsanweisung' OR mpg.kategorie = 'Sicherheitsdatenblatt'
 ORDER BY
  filename
;


-- version mpi-dcts
CREATE OR REPLACE VIEW view_ablageAll AS
 SELECT
  LPAD(bio.ablageID, 6, '0') AS ablageID,
  bio.kategorie AS kategorie,
  bio.bezeichnung AS bezeichnung,
  bio.file_filename AS filename,
  'bio' AS gruppe,
  bio.bearbeiter AS bearbeiter,
  bio.zeitstempel AS zeitstempel
 FROM
  mpidb_bio_chem.mpi_ablage AS bio
 WHERE
  bio.kategorie = 'Betriebsanweisung' OR bio.kategorie = 'Sicherheitsdatenblatt'
UNION ALL 
 SELECT
  LPAD(bpe.ablageID, 6, '0') AS ablageID,
  bpe.kategorie AS kategorie,
  bpe.bezeichnung AS bezeichnung,
  bpe.file_filename AS filename,
  'bpe' AS gruppe,
  bpe.bearbeiter AS bearbeiter,
  bpe.zeitstempel AS zeitstempel
 FROM 
  mpidb_bpe_chem.mpi_ablage AS bpe
 WHERE
  bpe.kategorie = 'Betriebsanweisung' OR bpe.kategorie = 'Sicherheitsdatenblatt'
UNION ALL
 SELECT
  LPAD(pcg.ablageID, 6, '0') AS ablageID,
  pcg.kategorie AS kategorie,
  pcg.bezeichnung AS bezeichnung,
  pcg.file_filename AS filename,
  'pcg' AS gruppe,
  pcg.bearbeiter AS bearbeiter,
  pcg.zeitstempel AS zeitstempel
 FROM
  mpidb_pcg_chem.mpi_ablage AS pcg
 WHERE
  pcg.kategorie = 'Betriebsanweisung' OR pcg.kategorie = 'Sicherheitsdatenblatt'
UNION ALL
 SELECT
  LPAD(pse.ablageID, 6, '0') AS ablageID,
  pse.kategorie AS kategorie,
  pse.bezeichnung AS bezeichnung,
  pse.file_filename AS filename,
  'pse' AS gruppe,
  pse.bearbeiter AS bearbeiter,
  pse.zeitstempel AS zeitstempel
 FROM
  mpidb_pse_chem.mpi_ablage AS pse
 WHERE
  pse.kategorie = 'Betriebsanweisung' OR pse.kategorie = 'Sicherheitsdatenblatt'
ORDER BY
 filename
;
