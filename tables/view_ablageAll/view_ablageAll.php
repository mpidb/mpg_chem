<?php

class tables_view_ablageALL { 

  // check login & editieren nicht erlaubt
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::NO_ACCESS();
    return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
  }

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    return $table;
  }

  // glanceview
  function getTitle(&$record) {
    return $record->strval('filename').' : '.$record->strval('bezeichnung');
  }

  // link nach Substanz
  function substanz__renderCell( &$record ) {
    $subst = $record->strval('substanz');
    $tabID = $record->strval('tabID');
    $subdb = $record->strval('gruppe').'_chem';
    $table = 'mpi_chemstoff';
    return '<div style="white-space:nowrap"><a href="/'.$subdb.'/index.php?-table='.$table.'&-action=browse&tabID='.$tabID.'">'.$subst.'</a></div>';
  }

  function filename__renderCell( &$record ) {
    $table = 'mpi_ablage';
    $field = 'ablageID';
    $tabID = $record->strval($field);
    $subdb = $record->strval('gruppe').'_chem';
    $name  = $record->strval('filename');
    return '<div style="text-align:right"><a href="/'.$subdb.'/index.php?-action=getBlob&-table='.$table.'&-field=file&-index=0&'.$field.'='.$tabID.'">'.$name.'</a></div>';
  }

   // Zeitanzeige aendern
  function zeitstempel__display(&$record) {
    if ($record->val('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

}
?>
