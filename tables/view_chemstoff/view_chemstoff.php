<?php

class tables_view_chemstoff { 

  // only authenticated user; edit in view not allowed; check role lower READ ONLY
  function getRoles(&$record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return 'NO ACCESS';
    return 'READ ONLY';
  }

  function block__before_menus() {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::NO_ACCESS();
    $urlAdd = DATAFACE_SITE_HREF."?-action=new&-table=mpi_chemstoff";
    echo   '<a class="contentActions" href='.$urlAdd.' title="Chemiestoff hinzuf&uuml;gen"><img src="/xataface/images/add_icon.gif" alt="" width="17" height="17"> Neuer Datensatz </a>';
    $urlAdd = DATAFACE_SITE_HREF."?-table=view_chemInv";
    echo   '<a class="contentActions" href='.$urlAdd.' title="Reduzierte Ansicht für Inventur Lagerbestand"><img src="images/inventur.png" alt="" width="14" height="14"> Inventur Lagerbestand </a>';
  }

  function block__after_result_list_content() {
    echo 'Lagerbestand hat den Mindestwert ';
    echo '<span style="background-color:#fcc;"> unterschritten. </span>&nbsp;|&nbsp;';
    echo '<span style="background-color:#ffc;"> erreicht. </span>&nbsp;|&nbsp;';
    echo '<span style="background-color:#cff;"> fast erreicht. </span>&nbsp;|&nbsp;';
    echo '<span style="background-color:#cfc;"> noch nicht erreicht. </span>&nbsp;|&nbsp;';
    echo '<span> nicht ueberwacht. </span>&nbsp;';
  }

  // setze Farbe in listenansicht entsprechend Minwert zu Menge
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    $menge = $record->val('anzahl');
    $min   = $record->val('min');
    $near  = $min + 1;
    if (($min == NULL) OR ($menge == NULL)) return $table;
    elseif ($menge <  $min)  return $table.'rot';
    elseif ($menge == $min)  return $table.'gelb';
    elseif ($menge == $near) return $table.'cyan';
    elseif ($menge >  $min)  return $table.'gruen';
    return $table;
  }

  // tabID mit nullen auffuellen solange noch nicht in DB geaendert
  function tabID__display(&$record) {
    $tabID = $record->strval('tabID');
    return str_pad($tabID, 5, 0, STR_PAD_LEFT);
  }

  // link nach Substanz
  function substanz__renderCell(&$record) {
    $subst = $record->strval('substanz');
    $tabID = $record->strval('tabID');
    $table = 'mpi_chemstoff';
    return '<div style="white-space:nowrap"><a href="'.DATAFACE_SITE_HREF.'?-table='.$table.'&-action=browse&tabID='.$tabID.'">'.$subst.'</a></div>';
  }

  function anzahl__renderCell(&$record) {
    return '<div style="text-align:right">'.$record->display('anzahl').'</div>';
  }

  function einheit__renderCell(&$record) {
    return '<div style="text-align:right">'.$record->display('einheit').'</div>';
  }

  // Umbruch in Liste verhindern
  function cas__renderCell(&$record) {
    return '<div style="white-space:nowrap">'.$record->display('cas').'</div>';
  }

  // Fake: weil '<' nicht dargestellt
  function reinheit__renderCell(&$record) {
    return $record->display('reinheit');
  }

  // link zum file und zugriff auf lokalen blob, wenn keine ablage exist
  function bFileID__renderCell( &$record ) {
    $ablageID  = $record->val('bFileID');
    $table = 'mpi_ablage';
    $sql = "SELECT file_filename FROM $table WHERE ablageID='$ablageID'";
    list($fname) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<a href="'.DATAFACE_SITE_HREF.'?-action=getBlob&-table='.$table.'&-field=file&ablageID='.$ablageID.'">'.$fname.'</a>';
  }

  // link zum file und zugriff auf lokalen blob, wenn keine ablage exist
  function sFileID__renderCell( &$record ) {
    $ablageID  = $record->val('sFileID');
    $table = 'mpi_ablage';
    $sql = "SELECT file_filename FROM $table WHERE ablageID='$ablageID'";
    list($fname) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return '<a href="'.DATAFACE_SITE_HREF.'?-action=getBlob&-table='.$table.'&-field=file&ablageID='.$ablageID.'">'.$fname.'</a>';
  }

  function valuelist__cmr() {
    return array(0=>'nein', 1=>'ja');
  }

  function valuelist__anweisung() {
    return array(0=>'nein', 1=>'ja');
  }

  // Formatiere Zeitstempel auf Deutsch
  function zeitstempel__display(&$record) {
    if ($record->strval('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

}
?>
