-- mit menge pro lager pro chemstoff und nicht NULL und > 0
-- ikert - 20160713 - wunsch
CREATE OR REPLACE VIEW view_chemstoff AS
SELECT
  chem.*,
  vfluss.anzahl AS anzahl,
  (SELECT LPAD(lagerID,6,'0') FROM mpi_lagerort WHERE lagerID = vfluss.lagerID) AS lagerID
 FROM
  mpi_chemstoff AS chem
  LEFT JOIN view_mengenfluss as vfluss ON vfluss.chemID = chem.tabID
 WHERE
  vfluss.anzahl IS NOT NULL AND vfluss.anzahl > 0
 ORDER BY
  substanz, reinheit, einheit, lagerID
;

-- mit menge pro lager pro chemstoff und nicht '0'
CREATE OR REPLACE VIEW view_chemstoff AS
SELECT
  chem.*,
  vfluss.anzahl AS anzahl,
  (SELECT LPAD(lagerID,6,'0') FROM mpi_lagerort WHERE lagerID = vfluss.lagerID) AS lagerID
 FROM
  mpi_chemstoff AS chem
  LEFT JOIN view_mengenfluss as vfluss ON vfluss.chemID = chem.tabID
 WHERE
  vfluss.anzahl IS NOT NULL 
 ORDER BY
  substanz, reinheit, einheit, lagerID
;

