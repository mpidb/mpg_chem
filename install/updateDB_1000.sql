-- run: mysql -u root -p mpidb_<grp>_chem < updateDB.sql
-- UPDATES immer von der niefrigsten bis zur hoechsten version ausfuehren
-- damit IF benutzt werden kann, wird eine prozedur erzeugt und am ende ausgefuehrt
-- 
-- views, funcs, procs nach moeglichkeit nur einmal in der max version ausfuehren

USE mpidb_mpg_chem;
DROP PROCEDURE IF EXISTS proc_update;
DELIMITER $$
CREATE PROCEDURE proc_update()
proc_label: BEGIN

-- CHANGES V1.0.00 :
-- *****************
-- db::version   - versionierung einschalten
-- init version
IF ( SELECT MAX(version) FROM dataface__version ) = '0' THEN
 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('1000');
END IF;

-- mindest version vorhanden
 IF ( SELECT MAX(version) FROM dataface__version ) < '1000' THEN
 LEAVE proc_label;
END IF;

-- CHANGES V1.0.01 :
-- *****************
-- fs::rsync     - pse_chem

 IF ( SELECT MAX(version) FROM dataface__version ) < '1001' THEN
  -- change primaer key
  ALTER TABLE mpi_lagerort CHANGE tabID lagerID SMALLINT(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;

  CREATE TABLE IF NOT EXISTS con_chemLager (
   conID smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
   chemID smallint(6) NOT NULL,
   lagerID smallint(6) unsigned zerofill NOT NULL,
   PRIMARY KEY (conID),
   UNIQUE KEY bestellID_chemID (lagerID, chemID),
   KEY chemID (chemID),
   KEY bestellID (lagerID)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

  ALTER TABLE con_chemLager
   ADD CONSTRAINT con_lager FOREIGN KEY (lagerID) REFERENCES mpi_lagerort  (lagerID) ON DELETE CASCADE,
   ADD CONSTRAINT con_chem  FOREIGN KEY (chemID)  REFERENCES mpi_chemstoff (tabID)   ON DELETE CASCADE;

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1001');
 END IF;


-- CHANGES V1.0.02 :
-- *****************

 IF ( SELECT MAX(version) FROM dataface__version ) < '1002' THEN

  -- db::con_chemLager - fuelle tabelle
  INSERT IGNORE INTO con_chemLager (chemID, lagerID) SELECT chem.tabID AS chemID, (SELECT lagerID FROM mpi_lagerort WHERE ablageort = chem.ablageort) AS lagerID FROM mpi_chemstoff AS chem WHERE chem.ablageort IS NOT NULL AND chem.ablageort <> '' ORDER BY lagerID; 

  -- db::list_reiter - add con_chemLager, add view_chemAll, change view_chemstoff
  INSERT IGNORE INTO list_reiter (reiter, kategorie, favorit, history, bedeutung) VALUES ('con_chemLager','Zuordnung','0','0','Mehrfachverbindung Chemiestoff-Lagerort');
  UPDATE list_reiter SET reiter = 'view_chemAll' WHERE list_reiter.autoID = '300';
  INSERT IGNORE INTO list_reiter (reiter, kategorie, favorit, history, bedeutung) VALUES ('view_chemstoff', 'View', '1', '0', 'Zeige alle Stoffe mit Lager und Menge');
  UPDATE list_reiter SET favorit = '1' WHERE reiter = 'mpi_chemstoff';

  -- db::mpi_chemstoff - loesche alten ablageort
  ALTER TABLE mpi_chemstoff DROP FOREIGN KEY `mpi_chemstoff_ibfk_5`;
  ALTER TABLE mpi_chemstoff DROP ablageort;
  ALTER TABLE mpi_chemstoff__history DROP ablageort;
  ALTER TABLE mpi_chemstoff CHANGE cmr cmr TINYINT(1) NULL DEFAULT '0';
  ALTER TABLE mpi_chemstoff CHANGE gift gift TINYINT(1) NULL DEFAULT '0';
  ALTER TABLE mpi_chemstoff CHANGE menge menge SMALLINT(5) NULL DEFAULT '0';


  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1002');
 END IF;

-- CHANGES V1.1.00 :
-- *****************
-- build index suche
 -- fs::action.ini   - enable ;;[manage] ;;[my_profile]  in master/actions.ini
 -- fs::conf.ini     - add [_index] table
 -- db::con_lagerLgk - add 1:n lagerklasse -> lagerort

 IF ( SELECT MAX(version) FROM dataface__version ) < '1100' THEN

  -- create new con table lagerort-lagerklasse
  CREATE TABLE IF NOT EXISTS con_lagerLgk (
   conID smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
   lgk varchar(10) COLLATE utf8_unicode_ci NOT NULL,
   lagerID smallint(6) unsigned zerofill NOT NULL,
   PRIMARY KEY (conID),
   UNIQUE KEY lgk_lagerID (lgk, lagerID),
   KEY lgk (lgk),
   KEY lagerID (lagerID)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;
   ALTER TABLE con_lagerLgk ADD CONSTRAINT con_lagerLgk_lagerID FOREIGN KEY (lagerID) REFERENCES mpi_lagerort (lagerID) ON DELETE CASCADE;

  -- db::list_reiter - add con_lagerLgk
  INSERT INTO list_reiter (reiter, kategorie, favorit, history, bedeutung) VALUES ('con_lagerLgk', 'Zuordnung', '0', '1', 'Mehrfachzuordnung Lagerklasse zu Lagerort');

  -- db::con_lagerLgk - alte Version konvertieren
  INSERT INTO con_lagerLgk (lgk, lagerID) SELECT lgk, lagerID FROM mpi_lagerort WHERE lgk NOT LIKE '' AND lgk IS NOT NULL AND lgk != '--';
  ALTER TABLE mpi_lagerort DROP lgk;
  ALTER TABLE mpi_lagerort DROP ablageort;
  -- ALTER TABLE mpi_lagerort__history DROP lgk;

  -- db::mpi_mengenfluss - add lagerID
  ALTER TABLE mpi_mengenfluss ADD lagerID SMALLINT UNSIGNED ZEROFILL NULL AFTER chemID, ADD INDEX (lagerID);
  ALTER TABLE mpi_mengenfluss ADD FOREIGN KEY (lagerID) REFERENCES mpi_lagerort (lagerID) ON DELETE CASCADE ON UPDATE RESTRICT;
  ALTER TABLE mpi_mengenfluss CHANGE lagerID lagerID SMALLINT(6) UNSIGNED ZEROFILL NOT NULL;

  -- db::mpi_chemstoff - del field menge
  ALTER TABLE mpi_chemstoff DROP menge;
  ALTER TABLE mpi_chemstoff__history DROP menge;

  -- db::mpi_mengefluss - change autoID
  ALTER TABLE mpi_mengenfluss CHANGE autoID autoID SMALLINT(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;

  -- view view_chemstoff - zeige alle Stoffe mit allen verb. Lagerorten
  CREATE OR REPLACE VIEW view_chemstoff AS
  SELECT
   chem.*,
   (SELECT SUM(fluss) FROM mpi_mengenfluss WHERE chemID = chem.tabID) AS anzahl,
   CONCAT(lag.raum,'/',lag.ablage) AS ablageort
  FROM
   mpi_chemstoff AS chem
   LEFT JOIN con_chemLager as con ON con.chemID = chem.tabID
   LEFT JOIN mpi_lagerort as lag ON lag.lagerID = con.lagerID
  ORDER BY
   substanz, reinheit, einheit, ablageort
  ;
  -- ende view

  -- view view_mengenfluss - zeige vorh. menge pro lager und chemiestoff
  CREATE OR REPLACE VIEW view_mengenfluss AS
  SELECT
   chemID,
   lagerID,
   SUM(fluss) AS anzahl
  FROM
   mpi_mengenfluss
  GROUP BY
   chemID, lagerID
  ;
  -- ende view

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1100');
 END IF;


-- CHANGES V1.1.01 :
-- *****************
 -- db::mpi_mengenfluss - alte daten ablageort in mengenfluss schreiben

 IF ( SELECT MAX(version) FROM dataface__version ) < '1101' THEN


  -- update ablageort in mengenfluss
  UPDATE mpi_mengenfluss AS flu LEFT JOIN con_chemLager AS con ON flu.chemID = con.chemID SET flu.lagerID = con.lagerID WHERE flu.lagerID = '0';


  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1101');
 END IF;


-- CHANGES V1.1.03 :
-- *****************
 -- db::view_chemstoff  - anzeige anzahl pro lagerort
 -- db::mpi_chemstoff   - anzeige anzahl pro chemiestoff

 IF ( SELECT MAX(version) FROM dataface__version ) < '1103' THEN

  -- view_chemiestoff anpassen
  CREATE OR REPLACE VIEW view_chemstoff AS
   SELECT
    chem.*,
    vfluss.anzahl AS anzahl,
    (SELECT CONCAT(raum,'/',ablage) FROM mpi_lagerort WHERE lagerID = vfluss.lagerID) AS ablageort
   FROM
    mpi_chemstoff AS chem
    LEFT JOIN view_mengenfluss as vfluss ON vfluss.chemID = chem.tabID
   ORDER BY
    substanz, reinheit, einheit, ablageort
  ;

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1103');
 END IF;


-- CHANGES V1.1.04 :
-- *****************
 -- db::mpi_nutzer - feld status aktiv/inaktiv
 -- fs::mpi_nutzer - feld status aktiv/inaktiv
 -- fs::valuelists.ini - status nutzer integrieren

 IF ( SELECT MAX(version) FROM dataface__version ) < '1104' THEN

  ALTER TABLE list_nutzer ADD status TINYINT(1) NOT NULL DEFAULT '1' AFTER betreuer ;

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1104');
 END IF;


-- CHANGES V1.1.05 :
-- *****************
 -- db::mpi_gefahr     - autoID
 -- fs::mpi_gefahr     - lgk static, sql join neu
 -- fs::valuelists.ini - verlinkt

 IF ( SELECT MAX(version) FROM dataface__version ) < '1105' THEN

  ALTER TABLE  mpi_gefahr CHANGE autoID autoID SMALLINT(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT ;

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1105');
 END IF;

 
-- CHANGES V1.1.06 - 2016-03-03
-- ****************************
 -- db::con_chemLager  - add links chemstoff-ablageort
 -- fs::conf.ini       - email parameter
 -- fs::custom.css     - color tables

 IF ( SELECT MAX(version) FROM dataface__version ) < '1106' THEN

  -- fuege in chemiestoff->ablageort alle schon mal per Mengenfluss benutzten Ablageorte als Verbindung hinzu
  INSERT IGNORE INTO con_chemLager( chemID, lagerID ) SELECT chemID, lagerID FROM mpi_mengenfluss WHERE lagerID > '0' GROUP BY lagerID, chemID;

  -- issue - <= 0 menge unterdrueckt
  CREATE OR REPLACE VIEW view_chemstoff AS
  SELECT
    chem.*,
    vfluss.anzahl AS anzahl,
    (SELECT LPAD(lagerID,6,'0') FROM mpi_lagerort WHERE lagerID = vfluss.lagerID) AS lagerID
   FROM
    mpi_chemstoff AS chem
    LEFT JOIN view_mengenfluss as vfluss ON vfluss.chemID = chem.tabID
   WHERE
    vfluss.anzahl IS NOT NULL
   ORDER BY
    zeitstempel
  ;


  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1106');
 END IF;


-- CHANGES V1.1.07 - 2016-03-30
-- ****************************
 -- db::mpi_ablage  - change foreign key to set null

 IF ( SELECT MAX(version) FROM dataface__version ) < '1107' THEN

  -- change foreign key on delete to SET NULL
  ALTER TABLE mpi_ablage DROP FOREIGN KEY mpi_ablage_ibfk_4 ;
  ALTER TABLE mpi_ablage DROP FOREIGN KEY mpi_ablage_ibfk_5 ;
  ALTER TABLE mpi_ablage ADD CONSTRAINT mpi_lieferant_tabID FOREIGN KEY (lieferID) REFERENCES mpi_lieferant (tabID) ON DELETE SET NULL ON UPDATE RESTRICT ;
  ALTER TABLE mpi_ablage ADD CONSTRAINT mpi_bestell_bestellID FOREIGN KEY (bestellID) REFERENCES mpi_bestell (bestellID) ON DELETE SET NULL ON UPDATE RESTRICT ;


  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1107');
 END IF;


-- CHANGES V1.1.08 - 2016-04-18
-- ****************************
 -- fs::rsync          - anpassungen fuer lieferID
 -- db::mpi_ablage     - change foreign key to set null
 -- db::mpi_bestell    - change lieferant to lieferID
 -- db::mpi_lieferant  - change tabID to lieferID

 IF ( SELECT MAX(version) FROM dataface__version ) < '1108' THEN

  -- drop unique
  ALTER TABLE mpi_ablage DROP INDEX kateg_filename_bez ;
  -- change lieferant to lieferID
  ALTER TABLE mpi_bestell ADD lieferID SMALLINT(6) UNSIGNED ZEROFILL NULL AFTER lieferant ;
  ALTER TABLE mpi_bestell ADD INDEX (lieferID) ;
  UPDATE mpi_bestell AS best LEFT JOIN mpi_lieferant AS lief ON best.lieferant = lief.lieferant SET best.zeitstempel = best.zeitstempel, best.lieferID = lief.tabID WHERE best.lieferant IS NOT NULL;
  ALTER TABLE mpi_bestell DROP FOREIGN KEY mpi_bestell_ibfk_2 ;
  IF NOT ( SELECT count(*) AS anz FROM mpi_bestell GROUP BY lieferID, bestellnummer ORDER BY anz DESC LIMIT 1) > '1' THEN
    -- ALTER TABLE mpi_bestell DROP INDEX liefer_bestell ;
    ALTER TABLE mpi_bestell ADD UNIQUE KEY lieferID_best (lieferID,bestellnummer) ;
  END IF;
  ALTER TABLE mpi_bestell DROP lieferant ;
  -- change tabID to lieferID 
  ALTER TABLE mpi_ablage DROP FOREIGN KEY mpi_lieferant_tabID ;
  ALTER TABLE mpi_lieferant CHANGE tabID  lieferID SMALLINT(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT ;
  ALTER TABLE mpi_bestell ADD CONSTRAINT mpi_lieferant_lieferID FOREIGN KEY (lieferID) REFERENCES mpi_lieferant (lieferID) ON DELETE NO ACTION ON UPDATE RESTRICT ;
  ALTER TABLE mpi_ablage DROP INDEX lieferID ;
  ALTER TABLE mpi_ablage ADD INDEX (lieferID) ;
  ALTER TABLE mpi_ablage ADD CONSTRAINT mpi_liefer_lieferID FOREIGN KEY (lieferID) REFERENCES mpi_lieferant (lieferID) ON DELETE NO ACTION ON UPDATE RESTRICT ;
  ALTER TABLE mpi_ablage DROP FOREIGN KEY mpi_liefer_lieferID ;
  ALTER TABLE mpi_ablage ADD CONSTRAINT mpi_liefer_lieferID FOREIGN KEY (lieferID) REFERENCES mpi_lieferant (lieferID) ON DELETE SET NULL ON UPDATE RESTRICT ;


  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1108');
 END IF;

-- CHANGES V1.1.09 - 2016-04-25
-- ****************************
 -- fs::rsync           - asi policy view BA und SDB
 -- db::view_chemALL    - add BA und SDB anderer gruppen

 IF ( SELECT MAX(version) FROM dataface__version ) < '1109' THEN

  -- change view_chemAll - mpg-version - evtl. erweitern
  -- in version 1113 geaendert


  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1109');
 END IF;


-- CHANGES V1.1.10 - 2016-04-28
-- ****************************
 -- fs::mpi_unterweisung   - change mpi_unterweisung
 -- db::mpi_unterweisung   - change mpi_unterweisung
 -- db::list_nutzer        - change autoID zu nutzerID
 -- db::con_chemNutzer     - add new table

 IF ( SELECT MAX(version) FROM dataface__version ) < '1110' THEN

  -- mpi_unterweisung anpassen
  ALTER TABLE mpi_unterweisung CHANGE bemerkung bemerkung VARCHAR(100) NULL ;
  ALTER TABLE mpi_unterweisung ADD nutzerID SMALLINT(6) UNSIGNED ZEROFILL NULL AFTER uweisID , ADD INDEX (nutzerID) ;
  ALTER TABLE list_nutzer CHANGE autoID nutzerID SMALLINT(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT ;
  ALTER TABLE mpi_unterweisung ADD CONSTRAINT nutzer_unterw_nutzerID FOREIGN KEY (nutzerID) REFERENCES list_nutzer (nutzerID
) ON DELETE CASCADE ON UPDATE RESTRICT ;
  UPDATE mpi_unterweisung AS unt SET unt.nutzerID = (SELECT nutzerID FROM list_nutzer WHERE nutzer = unt.nutzer), zeitstempel = zeitstempel WHERE unt.nutzer IS NOT NULL ;
  ALTER TABLE mpi_unterweisung DROP FOREIGN KEY  mpi_unterweisung_ibfk_2 ;
  ALTER TABLE mpi_unterweisung DROP nutzer ;
  ALTER TABLE mpi_unterweisung CHANGE nutzerID nutzerID SMALLINT(6) UNSIGNED ZEROFILL NOT NULL ;
  ALTER TABLE mpi_unterweisung CHANGE ablageID ablageID SMALLINT(6) UNSIGNED ZEROFILL NOT NULL ;
  ALTER TABLE mpi_unterweisung ADD UNIQUE KEY nutzer_ablage_datum (nutzerID,ablageID,datum) ;

  -- add table con_chemNutzer
  CREATE TABLE IF NOT EXISTS con_chemNutzer (
    conID smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
    chemID smallint(6) NOT NULL,
    nutzerID smallint(6) unsigned zerofill NOT NULL,
    PRIMARY KEY (conID),
    UNIQUE KEY chem_nutzer (chemID,nutzerID),
    KEY nutzerID (nutzerID),
    KEY chemID (chemID)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
  ALTER TABLE con_chemNutzer
    ADD CONSTRAINT con_chemNutzer_nutzerID FOREIGN KEY (nutzerID) REFERENCES list_nutzer (nutzerID) ON DELETE CASCADE,
    ADD CONSTRAINT con_chemNutzer_chemID FOREIGN KEY (chemID) REFERENCES mpi_chemstoff (tabID) ON DELETE CASCADE;

  INSERT IGNORE INTO con_chemNutzer (chemID,nutzerID) SELECT chem.tabID AS chemID, nut.nutzerID AS nutzerID FROM mpi_chemstoff AS chem LEFT JOIN list_nutzer AS nut ON chem.nutzer = nut.nutzer WHERE chem.nutzer IS NOT NULL AND chem.nutzer != '--' ;

  INSERT IGNORE INTO list_reiter (reiter, kategorie, favorit, history, bedeutung) VALUES ('con_chemNutzer','Zuordnung','0','0','Mehrfachverbindung Chemiestoff-Nutzer');
  ALTER TABLE mpi_chemstoff DROP FOREIGN KEY mpi_chemstoff_ibfk_4 ;
  ALTER TABLE mpi_chemstoff DROP nutzer ;
  DELETE FROM list_nutzer WHERE nutzer = '--' ;

  -- view ausfuehren wegen del nutzer
  CREATE OR REPLACE VIEW view_chemstoff AS
   SELECT
    chem.*,
    vfluss.anzahl AS anzahl,
    (SELECT LPAD(lagerID,6,'0') FROM mpi_lagerort WHERE lagerID = vfluss.lagerID) AS lagerID
   FROM
    mpi_chemstoff AS chem
    LEFT JOIN view_mengenfluss as vfluss ON vfluss.chemID = chem.tabID
   WHERE
    vfluss.anzahl IS NOT NULL
   ORDER BY
    substanz, reinheit, einheit, lagerID
  ;


  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1110');
 END IF;


-- CHANGES V1.1.11 - 2016-04-29
-- ****************************
 -- fs::con_ablageBest     - new table
 -- fs::con_ablageLief     - new table
 -- db::view_ablageALL  - alle duerfen BA und SDB anderer gruppen sehen
 -- db::con_ablageBest     - new table
 -- db::con_ablageLief     - new table
 -- db::list_reiter     - add table view_ablageAll

 IF ( SELECT MAX(version) FROM dataface__version ) < '1111' THEN


  -- view_ablageALL  - alle duerfen BA und SDB anderer gruppen sehen
  CREATE OR REPLACE VIEW view_ablageAll AS
   SELECT
    LPAD(mpg.ablageID, 6, '0') AS ablageID,
    mpg.kategorie AS kategorie,
    mpg.bezeichnung AS bezeichnung,
    mpg.file_filename AS filename,
    'mpg' AS gruppe,
    mpg.bearbeiter AS bearbeiter,
    mpg.zeitstempel AS zeitstempel
   FROM
    mpidb_mpg_chem.mpi_ablage AS mpg
   WHERE
    mpg.kategorie = 'Betriebsanweisung' OR mpg.kategorie = 'Sicherheitsdatenblatt'
   ORDER BY
    filename
   ;

  -- new m:n table bestell-ablage 
  CREATE TABLE IF NOT EXISTS con_ablageBest (
    conID smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
    ablageID smallint(6) unsigned zerofill NOT NULL,
    bestellID smallint(6) unsigned zerofill NOT NULL,
    PRIMARY KEY (conID),
    UNIQUE KEY ablage_bestell (ablageID,bestellID),
    KEY bestellID (bestellID),
    KEY ablageID (ablageID)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

  ALTER TABLE con_ablageBest
    ADD CONSTRAINT conAblBest_bestell_ID FOREIGN KEY (bestellID) REFERENCES mpi_bestell (bestellID) ON DELETE CASCADE ,
    ADD CONSTRAINT conAblBest_ablage_ID FOREIGN KEY (ablageID) REFERENCES mpi_ablage (ablageID) ON DELETE CASCADE ;

  INSERT con_ablageBest (ablageID, bestellID) SELECT ablageID, bestellID FROM mpi_ablage WHERE ablageID IS NOT NULL AND bestellID IS NOT NULL ;

  -- new m:n table liefer-ablage 
  CREATE TABLE IF NOT EXISTS con_ablageLief (
    conID smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
    ablageID smallint(6) unsigned zerofill NOT NULL,
    lieferID smallint(6) unsigned zerofill NOT NULL,
    PRIMARY KEY (conID),
    UNIQUE KEY ablage_liefer (ablageID,lieferID),
    KEY lieferID (lieferID),
    KEY ablageID (ablageID)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

  ALTER TABLE con_ablageLief
    ADD CONSTRAINT conAblLief_liefer_ID FOREIGN KEY (lieferID) REFERENCES mpi_lieferant (lieferID) ON DELETE CASCADE ,
    ADD CONSTRAINT conAblLief_ablage_ID FOREIGN KEY (ablageID) REFERENCES mpi_ablage (ablageID) ON DELETE CASCADE ;

  INSERT con_ablageLief (ablageID, lieferID) SELECT ablageID, lieferID FROM mpi_ablage WHERE ablageID IS NOT NULL AND lieferID IS NOT NULL ;

  -- add tables in list_reiter
  INSERT IGNORE INTO list_reiter (reiter, kategorie, favorit, history, bedeutung) VALUES ('view_ablageAll', 'View', '1', '0', 'Betriebsanw. und Sicherh.-Datenbl. andere Gruppen');
  INSERT IGNORE INTO list_reiter (reiter, kategorie, favorit, history, bedeutung) VALUES ('con_ablageBest','Zuordnung','0','0','Mehrfachverbindung Ablage-Bestellnr.');
  INSERT IGNORE INTO list_reiter (reiter, kategorie, favorit, history, bedeutung) VALUES ('con_ablageLief','Zuordnung','0','0','Mehrfachverbindung Ablage-Lieferant');

  -- change foreign key name
  ALTER TABLE mpi_bestell DROP FOREIGN KEY mpi_lieferant_lieferID ;
  ALTER TABLE mpi_bestell ADD CONSTRAINT bestell_lieferant_ID FOREIGN KEY (lieferID) REFERENCES mpi_lieferant (lieferID) ON DELETE CASCADE ON UPDATE RESTRICT ;

  -- del fields bestell, liefer in ablage
  ALTER TABLE mpi_ablage DROP FOREIGN KEY mpi_bestell_bestellID ;
  ALTER TABLE mpi_ablage DROP bestellID ;
  ALTER TABLE mpi_ablage DROP FOREIGN KEY mpi_liefer_lieferID ;
  ALTER TABLE mpi_ablage DROP lieferID ;


  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1111');
 END IF;


-- CHANGES V1.1.12 - 2016-05-04
-- ****************************
 -- fs::con_ablageDate     - new table reminder
 -- fs::con_ablageDate     - new table reminder
 -- db::list_reiter        - add table con_ablageDate
 -- db::list_nachricht     - del table

 IF ( SELECT MAX(version) FROM dataface__version ) < '1112' THEN

  -- add table reminder Betriebsanweisung
  CREATE TABLE IF NOT EXISTS con_ablageDate (
    conID smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
    ablageID smallint(6) unsigned zerofill NOT NULL,
    reminder date NOT NULL,
    email varchar(256) COLLATE utf8_unicode_ci NOT NULL,
    PRIMARY KEY (conID),
    UNIQUE KEY ablage_date_mail (ablageID,reminder,email(100)),
    KEY ablageID (ablageID)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
  ALTER TABLE con_ablageDate ADD CONSTRAINT date_ablage_ID FOREIGN KEY (ablageID) REFERENCES mpi_ablage (ablageID) ON DELETE CASCADE ;

  INSERT IGNORE INTO list_reiter (reiter, kategorie, favorit, history, bedeutung) VALUES ('con_ablageDate','Zuordnung','1','0','Erinnerrung Ablauf Betriebsanweisung');

  -- del table nachricht
  ALTER TABLE mpi_chemstoff DROP FOREIGN KEY mpi_chemstoff_ibfk_2 ;
  DROP TABLE list_nachricht ;
  DELETE FROM list_reiter WHERE reiter = 'list_nachricht' ;
  ALTER TABLE mpi_chemstoff CHANGE nachricht nachricht VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ;
  UPDATE mpi_chemstoff SET nachricht = NULL, zeitstempel = zeitstempel WHERE nachricht = '--' ;

  ALTER TABLE con_ablageDate
   ADD bearbeiter VARCHAR(20) NULL AFTER email ,
   ADD zeitstempel TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER bearbeiter ;


  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1112');
 END IF;


-- CHANGES V1.1.13 - 2016-05-30 !!! mpg-version anders !!!
-- ****************************
 -- fs::rsync           - del field cmr in chemstoff
 -- db::mpi_chemstoff   - del field cmr in chemstoff
 -- db::view_chemstoff  - del field cmr in chemstoff
 -- db::view_gefahr     - add field cmr in chemstoff

 IF ( SELECT MAX(version) FROM dataface__version ) < '1113' THEN

  -- del field cmr mpi_chemstoff
  ALTER TABLE mpi_chemstoff DROP cmr ;

  -- view chemstoff erneuern
  CREATE OR REPLACE VIEW view_chemstoff AS
  SELECT
    chem.*,
    vfluss.anzahl AS anzahl,
    (SELECT LPAD(lagerID,6,'0') FROM mpi_lagerort WHERE lagerID = vfluss.lagerID) AS lagerID
   FROM
    mpi_chemstoff AS chem
    LEFT JOIN view_mengenfluss as vfluss ON vfluss.chemID = chem.tabID
   WHERE
    vfluss.anzahl IS NOT NULL
   ORDER BY
    substanz, reinheit, einheit, lagerID
  ;

  -- new field cmr - version mpg stand alone
  CREATE OR REPLACE VIEW view_gefahr AS
   SELECT
    '000001' AS autoID,
    'keine' AS kategorie,
    'H000' AS hsatz,
    'Noch nicht verlinkt nach mpg_gfk.list_gefahr' AS merkmal,
    '0' AS cmr,
    '0' AS anweisung,
    '0' AS lgk
   ;

  -- new view ohne cmr  
  CREATE OR REPLACE VIEW view_chemAll AS
   SELECT
    mpg.tabID AS tabID,
    mpg.substanz AS substanz,
    mpg.reinheit AS reinheit,
    mpg.lgk AS lgk,
    mpg.cas AS cas,
    mpg.einheit AS einheit,
    LPAD(mpg.bFileID, 6, '0') AS bFileID,
    (SELECT file_filename FROM mpidb_mpg_chem.mpi_ablage WHERE mpg.bFileID = ablageID) AS bFilename,
    LPAD(mpg.sFileID, 6, '0') AS sFileID,
    (SELECT file_filename FROM mpidb_mpg_chem.mpi_ablage WHERE mpg.sFileID = ablageID) AS sFilename,
    'mpg' AS gruppe,
    mpg.bearbeiter AS bearbeiter,
    mpg.zeitstempel AS zeitstempel
   FROM mpidb_mpg_chem.mpi_chemstoff AS mpg
  ;

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1113');
 END IF;


-- CHANGES V1.1.14 - 2016-06-09
-- ****************************
 -- fs::conf.ini        - change comment # to ;
 -- fs::cronjob         - update to mysqli driver

 IF ( SELECT MAX(version) FROM dataface__version ) < '1114' THEN

  -- nur file version

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1114');
 END IF;


-- CHANGES V1.1.15 - 2016-06-13
-- ****************************
 -- fs::conf         - link nach master geloescht, Sortierung von fields.ini nach ApplicationDelegate.php
 -- fs::fields.ini   - sql order raus

 IF ( SELECT MAX(version) FROM dataface__version ) < '1115' THEN

  -- nur file version

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1115');
 END IF;


-- CHANGES V1.1.16 - 2016-06-17
-- ****************************
 -- fs::list_nutzer  - issue valuelists nutzerID
 -- db::list_nutzer  - foreign key nutzer = no action

 IF ( SELECT MAX(version) FROM dataface__version ) < '1116' THEN

  -- foreign key nutzer = no action
  ALTER TABLE list_nutzer DROP FOREIGN KEY list_nutzer_ibfk_2 ;
  ALTER TABLE list_nutzer ADD CONSTRAINT list_nutzer_nutzer FOREIGN KEY (betreuer) REFERENCES list_nutzer (nutzer) ON DELETE NO ACTION ON UPDATE CASCADE ;

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1116');
 END IF;


-- CHANGES V1.1.17 - 2016-06-27 MPG-Version
-- ****************************
 -- fs::view_gefahr  - add field kennzeichen
 -- db::view_gefahr  - run sql view

 IF ( SELECT MAX(version) FROM dataface__version ) < '1117' THEN

  -- db::view_gefahr  - run sql view
  -- CREATE OR REPLACE VIEW view_gefahr AS SELECT * FROM mpidb_mpg_gfk.list_gefahr ;
  -- new field kennzeichen - version mpg stand alone
  CREATE OR REPLACE VIEW view_gefahr AS
   SELECT
    '000001' AS autoID,
    'keine' AS kategorie,
    'H000' AS hsatz,
    'Noch nicht verlinkt nach mpg_gfk.list_gefahr' AS merkmal,
    '0' AS cmr,
    '0' AS anweisung,
    '0' AS lgk,
    '--' AS kennzeichen
   ;

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1117');
 END IF;


-- CHANGES V1.1.18 - 2016-11-21 !!! mpg-version !!!
-- ****************************
 -- fs|db::view_gefahr   - add field substition

 IF ( SELECT MAX(version) FROM dataface__version ) < '1118' THEN

  -- view_gefahr  - run sql view
  -- CREATE OR REPLACE VIEW view_gefahr AS SELECT * FROM mpidb_mpg_gfk.list_gefahr ;
  -- new field substition - version mpg stand alone
  CREATE OR REPLACE VIEW view_gefahr AS
   SELECT
    '000001' AS autoID,
    'keine' AS kategorie,
    'H000' AS hsatz,
    'Noch nicht verlinkt nach mpg_gfk.list_gefahr' AS merkmal,
    '0' AS cmr,
    '0' AS anweisung,
    '0' AS substition,
    '0' AS lgk,
    '--' AS kennzeichen
   ;

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1118');
 END IF;


-- CHANGES V1.1.19 - 2016-11-23
-- ****************************
 -- fs|db::view_chemInv   - add table for inventory chemstore

 IF ( SELECT MAX(version) FROM dataface__version ) < '1119' THEN

  -- create view view_chemInv
  CREATE OR REPLACE VIEW view_chemInv AS
  SELECT
    (SELECT raum FROM mpi_lagerort WHERE lagerID = vfluss.lagerID) AS Lagerort,
    (SELECT ablage FROM mpi_lagerort WHERE lagerID = vfluss.lagerID) AS Ablage,
    LPAD(chem.tabID,6,'0') AS ChemID,
    chem.substanz AS Substanz,
    chem.reinheit AS Reinheit,
    chem.lgk AS Lagerklasse,
    chem.einheit AS Entnahmegr,
    vfluss.anzahl AS Menge,
    NULL AS vorhanden,
    NULL AS Bemerkung
   FROM
    mpi_chemstoff AS chem
    LEFT JOIN view_mengenfluss as vfluss ON vfluss.chemID = chem.tabID
   WHERE
    vfluss.anzahl IS NOT NULL AND vfluss.anzahl > 0
   ORDER BY
    Lagerort, Ablage, Substanz, Reinheit, Einheit
  ;

  -- db::list_reiter - add view_chemInv
  INSERT IGNORE INTO list_reiter (reiter, kategorie, favorit, history, bedeutung) VALUES ('view_chemInv','View','0','0','Reduzierte Ansicht fuer den export fuer die Lagerinventur');

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1119');
 END IF;


-- CHANGES V1.1.20 - 2017-06-20
-- ****************************
 -- db::list_reiter   - unique reiter and elminate double view_ablageAll
 -- fs::mpi_ablage    - permission readable for special groups

 IF ( SELECT MAX(version) FROM dataface__version ) < '1120' THEN

  -- del double
  DELETE FROM `list_reiter` WHERE reiter = 'view_ablageAll' ;
  -- unique reiter
  ALTER TABLE list_reiter DROP INDEX reiter, ADD UNIQUE reiter (reiter) ;
  -- insert view_ablageAll
  INSERT IGNORE INTO list_reiter (reiter, kategorie, favorit, history, bedeutung) VALUES ('view_ablageAll', 'View', '1', '0', 'Betriebsanw. und Sicherh.-Datenbl. andere Gruppen') ;

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1120');
 END IF;


-- CHANGES V1.1.30 - 2018-08-14
-- ****************************
-- UPDATE: change Autorisierung von mpi_user nach sys_user mit Rollentabelle
-- db::list_reiter - Aenderung tabellen anpassen
-- fs::sys_user,list_rolle - Anpassung neue Benutzerverwaltung

IF ( SELECT MAX(version) FROM dataface__version ) < '1130' THEN

    -- create table list_role
    -- DROP TABLE IF EXISTS `list_role`;
    CREATE TABLE IF NOT EXISTS `list_role` (
      `rolID` smallint(6) UNSIGNED ZEROFILL NOT NULL,
      `role` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
      `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    -- insert default roles
    INSERT IGNORE INTO `list_role` (`rolID`, `role`, `description`) VALUES
    (000001, 'NO ACCESS', 'No_Access'),
    (000002, 'READ ONLY', 'view, list, calendar, view xml, show all, find, navigate'),
    (000003, 'EDIT', 'READ_ONLY and edit, new record, remove, import, translate, copy'),
    (000004, 'DELETE', 'EDIT and delete and delete found'),
    (000005, 'OWNER', 'DELETE except navigate, new, and delete found'),
    (000006, 'REVIEWER', 'READ_ONLY and edit and translate'),
    (000007, 'USER', 'READ_ONLY and add new related record'),
    (000008, 'ADMIN', 'DELETE and xml_view'),
    (000009, 'MANAGER', 'ADMIN and manage, manage_migrate, manage_build_index, and install');
    -- unique, pri
    ALTER TABLE `list_role` ADD PRIMARY KEY IF NOT EXISTS (`rolID`), ADD UNIQUE KEY IF NOT EXISTS `role` (`role`);
    -- auto_increment
    ALTER TABLE `list_role` MODIFY `rolID` smallint(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

    -- create table sys_user
    -- DROP TABLE IF EXISTS `sys_user`;
    CREATE TABLE IF NOT EXISTS `sys_user` (
     `logID` smallint(6) UNSIGNED ZEROFILL NOT NULL,
     `login` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
     `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
     `role` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
     `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
     `bearbeiter` varchar(20) COLLATE utf8_unicode_ci NULL,
     `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
     ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    -- unique, pri
    ALTER TABLE `sys_user`
     ADD PRIMARY KEY IF NOT EXISTS (`logID`) USING BTREE,
     ADD UNIQUE KEY  IF NOT EXISTS `login` (`login`) USING BTREE,
     ADD UNIQUE KEY  IF NOT EXISTS `email` (`email`),
     ADD KEY         IF NOT EXISTS `role` (`role`);
    -- auto_increment
    ALTER TABLE `sys_user` MODIFY `logID` smallint(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
    -- Constraint
    ALTER TABLE `sys_user` ADD CONSTRAINT `sysUser_listRole` FOREIGN KEY IF NOT EXISTS (`role`) REFERENCES `list_role` (`role`);

    -- mpg-version ohne externe DB (licman,inv,gfk,chem,user) - Auslieferzustand
    CREATE OR REPLACE VIEW view_user AS
     SELECT
      '000001' AS userID, 'mpg_local' AS login, 'MPG, Version (mpg_local)' AS sort
     ;

    -- create if not exist list_reiter
    CREATE TABLE IF NOT EXISTS `list_reiter` (
     `autoID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
     `reiter` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
     `kategorie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
     `favorit` tinyint(1) NOT NULL DEFAULT '0',
     `history` tinyint(1) NOT NULL DEFAULT '0',
     `bedeutung` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
     PRIMARY KEY (`autoID`),
     UNIQUE KEY `reiter` (`reiter`),
     KEY `kategorie` (`kategorie`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Reiterlinks fuer Xataface' AUTO_INCREMENT=9 ;

    -- create if not exist list_katReiter
    CREATE TABLE IF NOT EXISTS `list_katReiter` (
     `autoID` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
     `kategorie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
     PRIMARY KEY (`autoID`),
     UNIQUE KEY `kategorie` (`kategorie`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;
    -- add entries in list_katReiter
    UPDATE list_katReiter SET kategorie = 'Autorisierung' WHERE kategorie = 'Authorisierung';
    INSERT IGNORE INTO `list_katReiter` (`kategorie`) VALUES ('Autorisierung');

    -- add entries in list_reiter
    INSERT IGNORE INTO `list_reiter` (`reiter`, `kategorie`, `favorit`, `history`, `bedeutung`) VALUES
     ('sys_user', 'Autorisierung', 1, 1, 'Autorisierung und Berechtigung Benutzer'),
     ('list_role', 'Autorisierung', 1, 0, 'Liste aller Berechtigungen (Rollen)');
    UPDATE `list_reiter` SET `bedeutung` = 'Auswahlliste fuer aktive und nicht abgelaufene Benutzer' WHERE `reiter` = 'view_user';

    -- copy inserts from old mpi_users
    INSERT IGNORE INTO sys_user (login, password, role, email, bearbeiter, zeitstempel) SELECT username, password, role, email, 'import', zeitstempel FROM mpi_users;

    -- del old table mpi_users (if all done and work)
    DROP TABLE IF EXISTS `mpi_users`;
    DROP TABLE IF EXISTS `mpi_users__history`;
    DELETE FROM `list_reiter` WHERE `reiter` = 'mpi_users';

 TRUNCATE dataface__version ;
 INSERT INTO dataface__version (version) VALUES ('1130') ;

END IF;


END;
$$
DELIMITER ;

-- execute updates
CALL proc_update();

-- weil nur noch unter mariadb funktioniert, sonst kein import der DB fuer mysqldb moeglich
DROP PROCEDURE IF EXISTS proc_update;

