-- version mpg initial ohne mpg_chem
-- siehe Beispiel mpi-dcts in ../tables/view_gefahr/view_gefahr.sql
CREATE OR REPLACE VIEW mpidb_mpg_gfk.view_gefahr AS
 SELECT
  mpgT1.autoID,
  mpgT2.tabID,
  mpgT2.substanz,
  mpgT1.kategorie,
  'mpg' AS gruppe,
  mpgT1.bearbeiter,
  mpgT1.zeitstempel
 FROM
  mpidb_mpg_chem.mpi_gefahr AS mpgT1,
  mpidb_mpg_chem.mpi_chemstoff AS mpgT2
 WHERE
  mpgT1.chemID = mpgT2.tabID
;

-- version mpg mit mpg_chem
-- entsprechend erweitern/anpassen, wenn mehr chemie-db's existieren
-- siehe Beispiel mpi-dcts in ../tables/view_chemAll/view_chemAll.sql 
CREATE OR REPLACE VIEW mpidb_mpg_gfk.view_chemAll AS
 SELECT
  mpg.tabID AS tabID,
  mpg.substanz AS substanz,
  mpg.reinheit AS reinheit,
  mpg.lgk AS lgk,
  mpg.cas AS cas,
  mpg.einheit AS einheit,
  'mpg' AS gruppe,
  mpg.bearbeiter AS bearbeiter,
  mpg.zeitstempel AS zeitstempel
 FROM mpidb_mpg_chem.mpi_chemstoff AS mpg
;


