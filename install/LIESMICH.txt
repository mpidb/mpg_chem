###
Was muss ich anpassen:

!!! Vorhandene DB oder FileSystem immer vorher sichern !!!

1. DB-User anlegen mit sql Skript install/create_DB_User.sql
    Initialer Login ist admin mit Passwort admin
    sollte man irgendwann aendern

2. Datenbank anlegen mit sql Skript install/mpidb_mpg_<db>.sql
   bei Update install/updateDB_<version>.sql

3. domain, hostname, ssl, user, password unbedingt anpassen in
	conf.ini aendern
    user, pass in mysql-DB anpassen

4. ggf. cronjobs anlegen
    5 6 * * * php /var/www/mpg_chem/cronjobs/cronDatabase.php

5. wenn es eine seperate User-Datenbank gibt, dann folgende SQL-Skripte ausfuehren
    mysql -p -u root < install/joinChem2User.sql

6. Verbindungen zur Gefahren-Datenbanken herstellen und folgende SQL-Skripte ausfuehren
    mysql -p -u root < install/joinGfk2Chem.sql
    mysql -p -u root < install/joinChem2Gfk.sql

7. View's auf evtl. andere vorhandene Chemiedatenbanken
    mysql -p -u root < install/joinChem2Chem.sql

8. Autorisierung auf ldap umstellen
    Eintrag  #auth_type = basic     disable per Raute
    Eintrag   auth_type = ldap      enable Raute entfernen


Wie starte ich sql-Skripte?
 - per import in phpmyadmin oder
 - INITIAL: per Konsole mit 'mysql -p -u root < irgendEinName.sql
 - UPDATE:  per Konsole mit 'mysql -p -u root <datenbankname> < irgendEinName.sql

