-- version alone
-- fake auf sich selbst, also ohne db_chem's
CREATE OR REPLACE VIEW view_ablageAll AS
 SELECT 
  '000001' AS ablageID,
  'Betriebsanweisung' AS kategorie,
  'Irgendwer' AS lieferant,
  'Fake Eintrag wenn keine chemDB' AS bezeichnung,
  'noName' AS filename,
  'mpg' AS gruppe,
  'initial' AS bearbeiter,
  '2016-04-22 11:34:18' AS zeitstempel
;

CREATE OR REPLACE VIEW view_chemAll AS
 SELECT
  '1' AS tabID,
  'Fake Eintrag wenn keine chemDB' AS substanz,
  '-' AS reinheit,
  '-' AS lgk,
  '-' AS cas,
  '-' AS einheit,
  '0' AS bFileID,
  '-' AS bFilename,
  '0' AS sFileID,
  '-' AS sFilename,
  'mpg' AS gruppe,
  'initial' AS bearbeiter,
  '2016-05-03 11:34:18' AS zeitstempel
;


-- version mpg mit mpg_chem
-- entsprechend erweitern/anpassen, wenn mehr chemie-db's existieren
-- siehe Beispiel mpi-dcts (4db's) in ../tables/view_ablageAll/view_ablageAll.sql 
CREATE OR REPLACE VIEW view_ablageAll AS
 SELECT 
  LPAD(mpg.ablageID, 6, '0') AS ablageID,
  mpg.kategorie AS kategorie,
  (SELECT lieferant FROM mpi_lieferant WHERE lieferID = mpg.lieferID) AS lieferant,
  mpg.bezeichnung AS bezeichnung,
  mpg.file_filename AS filename,
  'mpg' AS gruppe,
  mpg.bearbeiter AS bearbeiter,
  mpg.zeitstempel AS zeitstempel
 FROM
  mpidb_mpg_chem.mpi_ablage AS mpg
 WHERE
  mpg.kategorie = 'Betriebsanweisung' OR mpg.kategorie = 'Sicherheitsdatenblatt'
 ORDER BY
  filename
;

CREATE OR REPLACE VIEW view_chemAll AS
 SELECT
  mpg.tabID AS tabID,
  mpg.substanz AS substanz,
  mpg.reinheit AS reinheit,
  mpg.lgk AS lgk,
  mpg.cas AS cas,
  mpg.einheit AS einheit,
  LPAD(mpg.bFileID, 6, '0') AS bFileID,
  (SELECT file_filename FROM mpidb_mpg_chem.mpi_ablage WHERE mpg.bFileID = ablageID) AS bFilename,
  LPAD(mpg.sFileID, 6, '0') AS sFileID,
  (SELECT file_filename FROM mpidb_mpg_chem.mpi_ablage WHERE mpg.sFileID = ablageID) AS sFilename,
  'mpg' AS gruppe,
  mpg.bearbeiter AS bearbeiter,
  mpg.zeitstempel AS zeitstempel
 FROM mpidb_mpg_chem.mpi_chemstoff AS mpg
;


