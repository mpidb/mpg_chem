#!/bin/bash
# schachi 2017-01-19
# export database for transport

  if test $# -ge 1; then
    if test $1 = "-h" || test $1 = "--help"; then
      echo "Execute $0 <db-name>"
      exit 0
    else
      db=$1
      path="/tmp"
    fi
  else
    path=`dirname $PWD`
    db=mpidb_`basename $path`
  fi

  echo -n "Export database $db to $PWD/$db.sql ? (y/[n]) : "
  read ok
  if test -z $ok; then ok="n"; fi
  if test "$ok" != "y"; then exit 0; fi

  /usr/bin/mysqldump -p --events --skip-extended-insert --skip-comments --databases $db > $PWD/$db.sql
