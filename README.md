## Einleitung ##

Die Datenbank Chemiestoff ist wie alle Datenbanken historisch entstanden und entsprechend unserem internen Gebrauch optimiert und entwickelt worden. Fuer die Benutzung der Gefahren- und Lagerklassen ist die Installation der uebergeordneten Datenbank mpg_gfk notwendig. Bitte den [Hinweis](https://gitlab.mpcdf.mpg.de/mpidb/mpg_chem/edit/master/README.md#warnhinweis) beachten.<br>
Die Chemiedatenbank ist fuer die Nutzung von mehreren Gruppen vorbereitet. D.h. allen greifen gemeinsam auf die Gefahren- und Lagerklassen zurueck, aber jede Gruppe hat dann ihre eigene DB (bei uns derzeit 4). Um die Lagerbestaende anderer Gruppen einzusehen ist eine gruppenuebergreifende Ansicht als extra Menuepunkt vorbereitet.<br>
<br>
Inhalte:
* Erfassung Chemiestoffe mit Reinheit,CAS,CMR,Giftig,Bindegroesse,Image,Bestellnumer,Lieferant usw.
* Lagerhaltung mit Mengenfluss und autom. Benachrichtigung bei Mindestunterschreitung
* Zuordnung Gefahrenklassen (in Verbindung mit DB mpg_gfk)
* autom. Zuordnung der Lagerklasse entsprechend Zuordnung Gefahrenklasse
* Ablageorte mit Raum und Ablage
* Zuordnung Gefahrenklasse mit autom. Bestimmung der hoechsten Lagerklasse enspr. Priorisierung
* Lieferanten mit Bestellnummer, Bindegroesse und Charge
* Ablage und Zuordnung von Betriebsanweisungen, Sicherheitsdatenblaetter und Zertifikaten
* Benachrichtigung fuer Ablauf Betriebsanweisung und Sicherheitsdatenblaetter 
* Status Lagerbestand per Color-Tag in Liste
* u.v.a.

## Projekt downloaden ##

**Evtl. vorh. DB und Filesystem vorher sichern**<br>
<br>
Entweder von common/db_export das tar-File herunterladen oder per Befehl
~~~bash
git clone https://gitlab.mpcdf.mpg.de/mpidb/mpg_chem.git
~~~
in das Wurzelverzeichnis des Webserver klonen.

## Installation ##

siehe LIEMICH.txt in Folder install<br>
Dort liegen auch alle noch notwendigen SQL-Skripte fuer die DB-Erstellung und weitere Verknuepfungen.

## Screenshot ##

<a href="install/db_chem_list.png" title="Liste Chemie Datenbank"><img src="install/db_chem_list.png" align="left" height="100" width="100"></a>
<a href="install/db_chem_view.png" title="View Chemie Datenbank"><img src="install/db_chem_view.png" align="left" height="100" width="100"></a>
<br><br><br><br>

## Warnhinweis ##

Die Benutzung der vorausgefuellten Tabellen fuer Gefahren- und Lagerklassen aus der DB Gefahrenklasse geschieht auf eigene Gefahr. Bitte pruefen sie unbedingt erst diese Tabellen bevor sie diese Datensaetze nutzen. Die Eintraege dienen nur als Vorlage und haben keinen Anspruch auf Vollstaendigkeit.

## Lizenzbedingungen ##

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.<br>
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.<br>  
See the GNU General Public License for more details.<br>
See also the file LICENSE.txt here
