<?php
/**
 * File: index.php
 * Description:
 * -------------
 *
 * This is an entry file for this Dataface Application.  To use your application
 * simply point your web browser to this file.
 */

  define('XATAFACE_DISABLE_PROXY_VIEWS',true);

  // Limit veraendern
  if ( !isset($_REQUEST['-limit']) ) {
     $_REQUEST['-limit'] = $_GET['-limit'] = '25';
  }

  // Sortierreihenfolge in Relationship aendern
  if ( !isset($_REQUEST['-related:sort']) and @$_REQUEST['-relationship'] == 'mengenfluss' ) {
    $_REQUEST['-related:sort'] = $_GET['-related:sort'] = 'zeitstempel desc';
  }

  if ( !isset($_REQUEST['-sort']) and @$_REQUEST['-table'] == 'mpi_ablage' ) {
    $_REQUEST['-sort'] = $_GET['-sort'] = 'zeitstempel desc';
  }

require_once '../xataface/dataface-public-api.php';
df_init(__FILE__, "/xataface")->display();

?>
