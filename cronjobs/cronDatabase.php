<?php

// CronJobs

  // stelle sicher das dieses Skript in einem Subdir liegt, normalerweise im Ordner cronjobs, sonst gibt es kausale Problem :-(
  // schachi 2016-06-28

  chdir(__DIR__);
  chdir('../');
  //print_r (realpath(__DIR__).' '.getcwd()."\n");
  if (!is_readable('conf.ini') ) trigger_error ('Error loading config file from here '.getcwd()."\n");

  $conf = array();
  $conf = parse_ini_file('conf.ini', true);
  //print_r ($conf);
  if ( !isset( $conf['_database'] )) trigger_error ('Error loading config file. No database specified.');
  //$dbinfo =& $conf['_database'];
  $dbinfo = $conf['_database'];
  if ( !is_array( $dbinfo ) || !isset($dbinfo['host']) || !isset( $dbinfo['user'] ) || !isset( $dbinfo['password'] ) || !isset( $dbinfo['name'] ) ) {
    trigger_error ('Error loading config file.  The database information was not entered correctly.');
  }
  $db = mysqli_connect($dbinfo['host'], $dbinfo['user'], $dbinfo['password'], $dbinfo['name'] );
  if ( !$db ) trigger_error ('Failed to connect to MySQL database: '.mysqli_connect_error($db)."\n");

  $debug = 0;  // 1 = ausgabe und keine Mail an User
  $mailto = 0; // 0 = keine Mail an Debugger
  //$mailto = 'schachi@mpi-magdeburg.mpg.de';

  // url
  $url = 'http';
  if ( isset($conf['_own']['ssl'])) {
    if ($conf['_own']['ssl'] == 1) $url = 'https';
  }
  $path = (basename(realpath('./')));
  $host = shell_exec("hostname -f | tr -d '\n'");
  $url  = $url.'://'.$host.'/'.$path;
  if ($debug) print_r($url."\n");


  // loesche alle <table>__history, welche nicht erwuenscht sind
  // Xataface hat nur einen globalen Schalter ON/OFF fuer history, aber wer braucht denn alle histories?
  $sql = "SELECT reiter FROM view_reiter WHERE reiter IN (SELECT CONCAT(lst.reiter, '__history') AS table_his FROM list_reiter AS lst LEFT JOIN view_reiter AS vReit ON lst.reiter = vReit.reiter WHERE lst.history = '0' AND vReit.table_type = 'BASE TABLE' AND lst.reiter NOT LIKE '%__history') AND table_type = 'BASE TABLE';";
  $result = mysqli_query($db, $sql) OR trigger_error ('Query failed: '.mysqli_error($db)."\n");
  $count = mysqli_num_rows($result);
  if ( $count >= 1 ) {
    while($row = mysqli_fetch_assoc($result)) {
      $table = $row['reiter'];
      $sql = "DROP TABLE IF EXISTS $table;";
      if ($debug) echo "$sql\n";
      mysqli_query($db, $sql) OR trigger_error ('Query failed: '.mysqli_error($db)."\n");
    }
  }


  // loesche alle Ablagen, welche keine Verbindung mehr haben, auch in Chemiestoff rueckwaerts
  // schachi 2016-05-02
  $sql = <<<EOT
  DELETE
   abl
  FROM
   mpi_ablage AS abl
   LEFT JOIN mpi_chemstoff AS chem ON (chem.bFileID = abl.ablageID OR chem.sFileID = abl.ablageID)
   LEFT JOIN con_ablageBest AS conB ON conB.ablageID = abl.ablageID
   LEFT JOIN con_ablageLief AS conL ON conL.ablageID = abl.ablageID
  WHERE
   chem.tabID IS NULL AND
   conB.conID IS NULL AND
   conL.conID IS NULL
EOT;
  mysqli_query($db, $sql) OR trigger_error ('Query failed: '.mysqli_error($db)."\n");


  // Script fuer automatisches senden von emails, wenn Minzahl erreicht oder unterschritten.
  // schachi 2015-07-07
  $sql = <<<EOT
  SELECT
   chem.tabID,
   chem.substanz,
   chem.reinheit,
   SUM(fluss.fluss) AS anzahl,
   chem.`min`,
   chem.einheit,
   chem.nachricht
  FROM
   mpi_chemstoff AS chem
   LEFT JOIN mpi_mengenfluss AS fluss ON chem.tabID = fluss.chemID
  WHERE (chem.nachricht LIKE '%@%') AND (chem.`min` > 0) AND ((SELECT SUM(fluss) FROM mpi_mengenfluss WHERE chemID = chem.tabID) <= chem.`min`)
  GROUP BY fluss.chemID
  ORDER BY substanz, reinheit, einheit
EOT;
  $result = mysqli_query($db, $sql) OR trigger_error ('Query failed: '.mysqli_error($db)."\n");
  $count = mysqli_num_rows($result);
  if ( $count >= 1 ) {
    $table = 'mpi_chemstoff';
    $field = 'tabID';
    $base  = $dbinfo['name'];
    while($row = mysqli_fetch_array($result)) {
      $tabID = $row[$field];
      $name  = utf8_encode ($row['substanz']);
      $reinh = $row['reinheit'];
      $mail  = $row['nachricht'];
      $anz   = $row['anzahl'];
      $min   = $row['min'];
      $binde = $row['einheit'];
      $body  = "INFO: Mindestmenge in $table erreicht";
      $text  = "Artikel: $name\nReinheit: $reinh\nAnzahl: $anz\nMinimum: $min\nGebindegr.: $binde\n";
      $link  = "Link Artikel: $url/index.php?-table=$table&-action=browse&$field=$tabID\n";
      $head  = "From: Database ".$base." <".$mail.">\n";
      $head .= "Content-Type: text/plain; charset=utf-8\n";
      $head .= "MIME-Version: 1.0\n";
      if ($debug) {
        print_r( "$mail\n$body\n${text}${link}\n$head\n" );
        if ($mailto != '0') mail( $mailto, $body, $text.$link, $head );
      } else {
        mail( $mail, $body, $text.$link, $head );
      }
    }
  }


  // Script fuer automatisches senden von emails, wenn Ablauf Betriebsanweisung erreicht oder ueberschritten.
  // schachi 2016-07-07
  function sendMail($db, $pre, $delay, $base, $table, $field, $url, $debug, $mailto) {
    $sql = <<<EOT
    SELECT
     con.conID,
     con.ablageID,
     con.reminder,
     con.email,
     con.zeitstempel,
     abl.kategorie,
     abl.file_filename
    FROM
     con_ablageDate AS con
     LEFT JOIN mpi_ablage AS abl ON con.ablageID = abl.ablageID
    WHERE (con.email LIKE '%@%') AND (con.reminder = ADDDATE( CURDATE(), $delay))
    ORDER BY con.reminder
EOT;
    if ($debug) print_r($delay."\n");
    $result = mysqli_query($db, $sql) OR trigger_error ('Query failed: '.mysqli_error($db)."\n");
    $count = mysqli_num_rows($result);
    //print_r ("\n$count $pre $delay\n");
    if ( $count >= 1 ) {
      $tbAbl = 'mpi_ablage';
      $field = 'conID';
      while($row = mysqli_fetch_array($result)) {
        $tabID = $row[$field];
        $ablID = $row['ablageID'];
        $kateg = $row['kategorie'];
        $file  = utf8_encode ($row['file_filename']);
        $mail  = $row['email'];
        $date  = $row['reminder'];
        $last  = $row['zeitstempel'];
        if ( $delay > 0 ) {
          $ende = 'ist in '.$delay.' Tagen.';
        } elseif ( $delay < 0 ) {
          $ende = 'ist überschritten!';
        } else {
          $ende = 'endet heute!';
        }
        $body  = "$pre Ablaufdatum $kateg $file $ende";
        $text  = "Kategorie: $kateg\nDateiname: $file\nLetzte Änderung: $last\nAblauf: $date\nBitte Datei oder Ablaufdatum erneuern.\n\n";
        $link  = "Link Ablaufdatum:\n$url/index.php?-table=$table&-action=browse&$field=$tabID\n";
        $file  = "Link Datei:\n$url/index.php?-table=$tbAbl&-action=getBlob&-field=file&ablageID=$ablID\n";
        $head  = "From: Database ".$base." <".$mail.">\n";
        $head .= "Content-Type: text/plain; charset=utf-8\n";
        $head .= "MIME-Version: 1.0\n";
        if ($debug) {
          print_r( "$mail\n$body\n${text}${link}.${file}\n$head\n" );
          if ($mailto != '0') mail( $mailto, $body, $text.$link.$file, $head );
        } else {
          mail( $mail, $body, $text.$link.$file, $head );
        }
      }
    }
  }

  if (!isset( $conf['_own']['notify'] )) $delay = 30; else $delay = $conf['_own']['notify'];
  $base = $dbinfo['name'];
  $table = 'con_ablageDate';
  $field = 'tabID';
  sendMail( $db, '[INFO]',    $delay, $base, $table, $field, $url, $debug, $mailto );
  sendMail( $db, '[TERMIN]',  '7',    $base, $table, $field, $url, $debug, $mailto );
  sendMail( $db, '[WICHTIG]', '0',    $base, $table, $field, $url, $debug, $mailto );
  sendMail( $db, '[WARNUNG]', '-7',   $base, $table, $field, $url, $debug, $mailto );


  mysqli_close($db);

?>
